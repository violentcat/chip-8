
#include "win32_uiToolkit.h"

namespace
{
    ULONG_PTR token(0);
    WCHAR wideString[256];
}

win32UIToolkit::win32UIToolkit(BITMAPINFO *bitmapInfo, void *pixelData)
{
    Gdiplus::GdiplusStartupInput i;
    GdiplusStartup(&token, &i, NULL);

    bitmapData = new Gdiplus::Bitmap(bitmapInfo, pixelData);
    drawObject = Gdiplus::Graphics::FromImage(bitmapData);
}

win32UIToolkit::~win32UIToolkit()
{
    delete drawObject;
    delete bitmapData;

    Gdiplus::GdiplusShutdown(token);
}

void win32UIToolkit::beginDraw()
{
    Gdiplus::Color blackness(0,0,0,0);
    drawObject->Clear(blackness);
}

void win32UIToolkit::renderText(const char *stringA, TextStyle *style, i32 xPos, i32 yPos)
{
    Gdiplus::PointF clientPos(static_cast<float>(xPos), static_cast<float>(yPos));
    Gdiplus::SolidBrush clientBrush(style->colour);        
    drawObject->DrawString(createCompatibleString(stringA), -1, &style->font, clientPos, &clientBrush);    
}

void win32UIToolkit::renderFilledRect(LineStyle *style, i32 xPos, i32 yPos, i32 width, i32 height)
{
    Gdiplus::SolidBrush clientBrush(style->colour);        
    drawObject->FillRectangle(&clientBrush, xPos, yPos, width, height);
}

void win32UIToolkit::endDraw()
{
    drawObject->Flush();
}

const WCHAR* win32UIToolkit::createCompatibleString(const char *stringDef)
{
    MultiByteToWideChar(CP_ACP, 0, stringDef, (int)strlen(stringDef), wideString, 256);
    wideString[strlen(stringDef)] = 0;
    return wideString;
}
