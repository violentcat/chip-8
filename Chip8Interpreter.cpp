
#include "Chip8Interpreter.h"
#include "Chip8Reference.h"

using namespace Chip8;

namespace
{
    u8 spriteSet[] = {0xF0, 0x90, 0x90, 0x90, 0xF0,        // 0                              
                      0x20, 0x60, 0x20, 0x20, 0x70,        // 1
                      0xF0, 0x10, 0xF0, 0x80, 0xF0,        // 2
                      0xF0, 0x10, 0xF0, 0x10, 0xF0,        // 3
                      0x90, 0x90, 0xF0, 0x10, 0x10,        // 4
                      0xF0, 0x80, 0xF0, 0x10, 0xF0,        // 5
                      0xF0, 0x80, 0xF0, 0x90, 0xF0,        // 6
                      0xF0, 0x10, 0x20, 0x40, 0x40,        // 7
                      0xF0, 0x90, 0xF0, 0x90, 0xF0,        // 8
                      0xF0, 0x90, 0xF0, 0x10, 0xF0,        // 9
                      0xF0, 0x90, 0xF0, 0x90, 0x90,        // A
                      0xE0, 0x90, 0xE0, 0x90, 0xE0,        // B
                      0xF0, 0x80, 0x80, 0x80, 0xF0,        // C
                      0xE0, 0x90, 0x90, 0x90, 0xE0,        // D
                      0xF0, 0x80, 0xF0, 0x80, 0xF0,        // E
                      0xF0, 0x80, 0xF0, 0x80, 0x80};       // F

    u8 chip48SpriteSet[] = {0x3C, 0x7E, 0xE7, 0xC3, 0xC3, 0xC3, 0xC3, 0xE7, 0x7E, 0x3C,     // 0
                            0x18, 0x38, 0x58, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3C,     // 1
                            0x3E, 0x7F, 0xC3, 0x06, 0x0C, 0x18, 0x30, 0x60, 0xFF, 0xFF,     // 2
                            0x3C, 0x7E, 0xC3, 0x03, 0x0E, 0x0E, 0x03, 0xC3, 0x7E, 0x3C,     // 3
                            0x06, 0x0E, 0x1E, 0x36, 0x66, 0xC6, 0xFF, 0xFF, 0x06, 0x06,     // 4
                            0xFF, 0xFF, 0xC0, 0xC0, 0xFC, 0xFE, 0x03, 0xC3, 0x7E, 0x3C,     // 5
                            0x3E, 0x7C, 0xC0, 0xC0, 0xFC, 0xFE, 0xC3, 0xC3, 0x7E, 0x3C,     // 6
                            0xFF, 0xFF, 0x03, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x60, 0x60,     // 7
                            0x3C, 0x7E, 0xC3, 0xC3, 0x7E, 0x7E, 0xC3, 0xC3, 0x7E, 0x3C,     // 8
                            0x3C, 0x7E, 0xC3, 0xC3, 0x7F, 0x3F, 0x03, 0x03, 0x3E, 0x7C};    // 9
};


void Chip8Interpreter::setCompatibilityModes(u8 flags)
{
    compatibleMode = flags;
    displayCompatibilityMode = true;
}

bool Chip8Interpreter::isCPUState(u8 flags)
{
    return (currentCPUState & flags) ? true : false;
}

void Chip8Interpreter::setCPUState(u8 flags)
{
    currentCPUState |= flags;
    if (flags & asType(CPU_FLAGS::RESET_ACTIVE))
    {
        // When RESET goes high all registers are loaded with
        // their default state
        for (i8 loop = 0; loop < 16; loop++)
        {
            reg.Vx[loop] = 0;
        }

        reg.delay = 0;
        reg.sound = 0;
        reg.I = 0;
        reg.PC = asType(MEMMAP::PRG_START);
        reg.SP = 24;

        loadROMSprites(asType(MEMMAP::ROM_SPRITE));
        clearGraphicsMemory();        
    }        
}

void Chip8Interpreter::resetCPUState(u8 flags)
{
    currentCPUState &= ~flags;
}

void Chip8Interpreter::mapMemorySpace(void *memoryBlock, u16 start, i32 length)
{
    mainMemory = reinterpret_cast<u8 *>(memoryBlock);
    firstAddress = start;
    mappedMemorySize = length;
}

void Chip8Interpreter::invalidateMappedMemory()
{
    u8 *mappedByte = mainMemory;

    for (i32 loop = 0; loop < mappedMemorySize; loop++)
    {
        *mappedByte++ = 0xFF;
    }
}

void Chip8Interpreter::copyBlockToMapped(u16 addressStart, void *block, i32 blockLength)
{
    platformAssert(addressStart + blockLength < mappedMemorySize);

    u8 *mappedByte = mainMemory + addressStart;
    u8 *sourceByte = reinterpret_cast<u8 *>(block);
        
    for (i32 loop = 0; loop < blockLength; loop++)
    {
        *mappedByte++ = *sourceByte++;
    }
}

void Chip8Interpreter::copyBlockFromMapped(u16 addressStart, void *block, i32 blockLength)
{
}

void Chip8Interpreter::initGraphicsMemory(void *pixelData, i32 width, i32 height, i32 pitch)
{
    // Target format is always 32-bit with byte order BGRA in memory
    // X/Y format with 0,0 in bottom-left
    rgbaTarget = reinterpret_cast<u32 *>(pixelData);
    displayWidth = width;
    displayHeight = height;
    displayPitch = pitch;
}

void Chip8Interpreter::initSoundMemory(void *waveformBuffer, i32 samplesPerSecond)
{
    // Target format is always 16-bit stereo samples (little endian)
    // Left - Right - Left - Right - Left -Right etc
    sampleTarget = reinterpret_cast<i16 *>(waveformBuffer);
    sampleHz = samplesPerSecond;
    samplesPerHalfCycle = (sampleHz / toneFrequencyHz) / 2;
}

void Chip8Interpreter::simulationCycle(i32 instructionCount, ControllerState *inputState)
{
    // CPU held in reset so no registers should be accessed
    if (isCPUState(asType(CPU_FLAGS::RESET_ACTIVE)))
        return;
    
    // Process timer registers once per simulation cycle
    if (reg.delay)
    {
        --reg.delay;
    }
    if (reg.sound)
    {
        --reg.sound;
    }

    // When CPU halted (debug state) only the timers are processed
    if (isCPUState(asType(CPU_FLAGS::HALT)))
        return;

    // Process single-step state by retiring a single opcode and then returning
    // the CPU to HALT
    if (isCPUState(asType(CPU_FLAGS::STEP)))
    {
        resetCPUState(asType(CPU_FLAGS::STEP));
        setCPUState(asType(CPU_FLAGS::HALT));
        instructionCount = 1;
    }
    
    for (i32 stepValue = 0; stepValue < instructionCount; stepValue++)
    {        
        // TODO(KN): Should we provide a general memory mapping function for interpreter to
        // 'real' memory addresses
        u8 *physicalLoc = mainMemory + reg.PC;
        u8 opcodeType = physicalLoc[0];
        u8 parameters = physicalLoc[1];
        u16 pcIncrement = 2;

        switch(opcodeType >> 4)
        {
            case 0x0: // See Contained Decoder
            {
                switch(parameters >> 4)
                {
                    case 0xC: // scroll display N lines down
                    {
                        i8 lineCount = parameters & 0xF;

                        for (i32 yLoop = 0; yLoop < displayHeight-lineCount; yLoop++)
                        {
                            u8 *renderRowDst = reinterpret_cast<u8 *>(rgbaTarget) + (yLoop * displayPitch);
                            u8 *renderRowSrc = reinterpret_cast<u8 *>(rgbaTarget) + ((yLoop + lineCount) * displayPitch);

                            u32 *pixelAddrDst = reinterpret_cast<u32 *>(renderRowDst);
                            u32 *pixelAddrSrc = reinterpret_cast<u32 *>(renderRowSrc);                            

                            for (i32 xLoop = 0; xLoop < displayWidth; xLoop++)
                            {
                                *pixelAddrDst++ = *pixelAddrSrc++;
                            }
                        }

                        for (i32 yLoop = displayHeight-lineCount; yLoop < displayHeight; yLoop++)
                        {
                            u8 *renderRow = reinterpret_cast<u8 *>(rgbaTarget) + (yLoop * displayPitch);
                            u32 *pixelAddr = reinterpret_cast<u32 *>(renderRow);

                            for (i32 xLoop = 0; xLoop < displayWidth; xLoop++)
                            {
                                *pixelAddr++ = 0x00000000;
                            }
                        }                        
                    }
                    break;

                    default:
                    {
                        switch(parameters)
                        {
                            case 0xE0: // clear screen
                            {
                                clearGraphicsMemory();
                            }
                            break;
                    
                            case 0xEE: // return from call
                            {
                                reg.PC = stackMem[reg.SP++];
                                pcIncrement = 0;
                            }
                            break;

                            case 0xFB: // scroll display 4 pixels right
                            {
                                for (i32 yLoop = 0; yLoop < displayHeight; yLoop++)
                                {
                                    u8 *renderRow = reinterpret_cast<u8 *>(rgbaTarget) + (yLoop * displayPitch);
                                    u32 *pixelAddrSrc = reinterpret_cast<u32 *>(renderRow);
                                    u32 *pixelAddrDst = reinterpret_cast<u32 *>(renderRow) + 4;
                                    
                                    for (i32 xLoop = 0; xLoop < displayWidth-4; xLoop++)
                                    {
                                        *pixelAddrDst++ = *pixelAddrSrc++;
                                    }

                                    pixelAddrDst = reinterpret_cast<u32 *>(renderRow);
                                    for (i32 xLoop = 0; xLoop < 4; xLoop++)
                                    {
                                        *pixelAddrDst++ = 0x00000000;
                                    }
                                }                        
                            }
                            break;
                            
                            case 0xFC: // scroll display 4 pixels left
                            {
                                for (i32 yLoop = 0; yLoop < displayHeight; yLoop++)
                                {
                                    u8 *renderRow = reinterpret_cast<u8 *>(rgbaTarget) + (yLoop * displayPitch);
                                    u32 *pixelAddrDst = reinterpret_cast<u32 *>(renderRow);
                                    u32 *pixelAddrSrc = reinterpret_cast<u32 *>(renderRow) + 4;
                                    
                                    for (i32 xLoop = 0; xLoop < displayWidth-4; xLoop++)
                                    {
                                        *pixelAddrDst++ = *pixelAddrSrc++;
                                    }

                                    for (i32 xLoop = displayWidth-4; xLoop < displayWidth; xLoop++)
                                    {
                                        *pixelAddrDst++ = 0x00000000;
                                    }
                                }                        
                            }
                            break;
                            
                            case 0xFD: // exit Interpreter
                            {
                                setCPUState(asType(CPU_FLAGS::HALT));
                                pcIncrement = 0;
                            }
                            break;
                    
                            case 0xFE: // Disable extended screen mode
                            {
                                displayCompatibilityMode = true;
                            }
                            break;
                    
                            case 0xFF: // Enable extended screen mode
                            {
                                displayCompatibilityMode = false;
                            }
                            break;
                    
                            default:
                            {
                                setCPUState(asType(CPU_FLAGS::HALT));
                                pcIncrement = 0;
                            }
                            break;
                        }                        
                    }
                    break;
                }
            }
            break;
        
            case 0x1: // goto NNN
            {
                reg.PC = ((opcodeType & 0xF) << 8) | parameters;
                pcIncrement = 0;
            }
            break;

            case 0x2: // Call NNN
            {
                platformAssert(reg.SP);

                if (reg.SP)
                {
                    stackMem[--reg.SP] = reg.PC + 2;
                    reg.PC = ((opcodeType & 0xF) << 8) | parameters;
                    pcIncrement = 0;
                }
            }
            break;
        
            case 0x3: // Skip Next if (Vx == NN)
            {
                if (reg.Vx[opcodeType & 0xF] == parameters)
                    pcIncrement = 4;
            }
            break;

            case 0x4: // Skip Next if (Vx != NN)
            {
                if (reg.Vx[opcodeType & 0xF] != parameters)
                    pcIncrement = 4;
            }
            break;

            case 0x5: // Skip Next if (Vx == Vy)
            {
                if (reg.Vx[opcodeType & 0xF] == reg.Vx[parameters >> 4])
                    pcIncrement = 4;
            }
            break;
        
            case 0x6: // Vx = NN
            {
                reg.Vx[opcodeType & 0xF] = parameters;
            }
            break;

            case 0x7: // Vx += NN
            {
                reg.Vx[opcodeType & 0xF] += parameters;
            }
            break;

            case 0x8: // See Contained Decoder
            {
                switch(parameters & 0xF)
                {
                    case 0x0: // Vx = Vy
                    {
                        reg.Vx[opcodeType & 0xF] = reg.Vx[parameters >> 4];
                    }
                    break;

                    case 0x1: // Vx = Vx | Vy
                    {
                        reg.Vx[opcodeType & 0xF] |= reg.Vx[parameters >> 4];
                    }
                    break;
                    
                    case 0x2: // Vx = Vx & Vy
                    {
                        reg.Vx[opcodeType & 0xF] &= reg.Vx[parameters >> 4];
                    }
                    break;

                    case 0x3: // Vx = Vx ^ Vy
                    {
                        reg.Vx[opcodeType & 0xF] ^= reg.Vx[parameters >> 4];
                    }
                    break;

                    case 0x4: // Vx += Vy (with carry)
                    {
                        i32 promotedVx = reg.Vx[opcodeType & 0xF] + reg.Vx[parameters >> 4];
                        reg.Vx[0xF] =  promotedVx > 255 ? 1 : 0;
                        reg.Vx[opcodeType & 0xF] = static_cast<i8>(promotedVx & 0xFF);
                    }
                    break;

                    case 0x5: // Vx -= Vy (with !borrow)
                    {                        
                        bool borrowFlag = true;
                        if (reg.Vx[opcodeType & 0xF] > reg.Vx[parameters >> 4])
                        {
                            borrowFlag = false;
                        }
                   
                        reg.Vx[opcodeType & 0xF] -= reg.Vx[parameters >> 4];
                        reg.Vx[0xF] = (borrowFlag == true) ? 0 : 1;
                    }
                    break;

                    case 0x6: // Vx = Vy = Vy >> 1 (VF = pre-shift LSB of Vy)
                    {
                        if (compatibleMode & asType(COMPATIBILITY_FLAGS::SHIFT_USE_VY))
                        {
                            u8 tempVy = reg.Vx[parameters >> 4];                        
                            reg.Vx[0xF] = tempVy & 0x1;
                            reg.Vx[opcodeType & 0xF] = reg.Vx[parameters >> 4] = tempVy >> 1;                            
                        }
                        else
                        {
                            reg.Vx[0xF] = reg.Vx[opcodeType & 0xF] & 0x1;
                            reg.Vx[opcodeType & 0xF] >>= 1;
                        }
                    }
                    break;

                    case 0x7: // Vx = Vy - Vx (with !borrow)
                    {
                        bool borrowFlag = true;
                        if (reg.Vx[parameters >> 4] > reg.Vx[opcodeType & 0xF])
                        {
                            borrowFlag = false;
                        }
                                          
                        reg.Vx[opcodeType & 0xF] = reg.Vx[parameters >> 4] - reg.Vx[opcodeType & 0xF];
                        reg.Vx[0xF] = (borrowFlag == true) ? 0 : 1;
                    }
                    break;

                    case 0xE: // Vx = Vy = Vy << 1 (VF = pre-shift MSB of Vy)
                    {
                        if (compatibleMode & asType(COMPATIBILITY_FLAGS::SHIFT_USE_VY))
                        {
                            u8 tempVy = reg.Vx[parameters >> 4];                        
                            reg.Vx[0xF] = (tempVy >> 7) & 0x1;
                            reg.Vx[opcodeType & 0xF] = reg.Vx[parameters >> 4] = tempVy << 1;                                                        
                        }
                        else
                        {
                            reg.Vx[0xF] = (reg.Vx[opcodeType & 0xF] >> 7) & 0x1;
                            reg.Vx[opcodeType & 0xF] <<= 1;
                        }
                    }
                    break;
                    
                    default:
                    {
                        setCPUState(asType(CPU_FLAGS::HALT));
                        pcIncrement = 0;
                    }
                    break;
                }
            }
            break;

            case 0x9: // Skip Next if (Vx != Vy)
            {
                if (reg.Vx[opcodeType & 0xF] != reg.Vx[parameters >> 4])
                    pcIncrement = 4;
            }
            break;
            
            case 0xA: // I = NNN
            {
                reg.I = ((opcodeType & 0xF) << 8) | parameters;
            }
            break;

            case 0xB: // goto NNN + V0
            {
                reg.PC = (((opcodeType & 0xF) << 8) | parameters) + reg.Vx[0];
                pcIncrement = 0;
            }
            break;

            case 0xC: // Vx = RND & NN
            {
                reg.Vx[opcodeType & 0xF] = random() & parameters;
            }
            break;

            case 0xD: // Draw (Vx,Vy,N)
            {
                bool carryFlag = false;
                i32 modeWidth = displayWidth / 2;
                i32 modeHeight = displayHeight / 2;
            
                u8 *spriteData = mainMemory + reg.I;
                u8 xPos = reg.Vx[opcodeType & 0xF];
                u8 yPos = reg.Vx[parameters >> 4];
                i8 height = parameters & 0xF;
                i8 spriteBitWidth = 8;

                if (displayCompatibilityMode == false)
                {
                    modeWidth = displayWidth;
                    modeHeight = displayHeight;
                    if (height == 0)
                    {
                        // Fixed format 16x16 SChip sprite
                        height = 16;
                        spriteBitWidth = 16;
                    }
                }                

                while(height--)
                {
                    bool validWrite = (xPos < modeWidth && yPos < modeHeight) ? true : false;
                    
                    i32 xIter = xPos;
                    i32 yInvert = (modeHeight - 1) - yPos;
                               
                    u8 *renderRow = reinterpret_cast<u8 *>(rgbaTarget) + (yInvert * displayPitch);
                    u32 *pixelAddr = reinterpret_cast<u32 *>(renderRow) + xIter;

                    u8 spriteMask = 0x80;
                    i8 drawBits = spriteBitWidth;
                    while(spriteMask)
                    {                
                        if ((*spriteData & spriteMask) && validWrite == true)
                        {
                            if (*pixelAddr)
                            {
                                *pixelAddr = 0x00000000;
                                 carryFlag = true;
                            }
                            else
                            {
                                *pixelAddr = 0x00FFFFFF;
                            }                        
                        }

                        spriteMask >>= 1;
                        drawBits--;
                        if (!spriteMask && drawBits)
                        {
                            spriteMask = 0x80;
                            spriteData++;
                        }
                        
                        pixelAddr++;
                        if (++xIter == modeWidth)
                        {
                            if (compatibleMode & asType(COMPATIBILITY_FLAGS::WRAP_X_AXIS))
                            {
                                // Wrap X if sprite runs over edge of display
                                xIter = 0;
                                pixelAddr = reinterpret_cast<u32 *>(renderRow);
                            }
                            else
                            {
                                // X Wrap is off so no more pixels on this sprite row
                                spriteMask = 0;
                            }
                        }
                    }

                    spriteData++;
                    if (++yPos == modeHeight)
                    {
                        if (compatibleMode & asType(COMPATIBILITY_FLAGS::WRAP_Y_AXIS))
                        {                        
                            // Wrap Y if sprite runs off the bottom of the display
                            yPos = 0;
                        }
                        else
                        {
                            // Y Wrap is off so no more rows for this sprite
                            height = 0;
                        }
                    }
                }

                // Set carry flag if any previously set pixel was cleared as a result
                // of sprite draw (acts as collision detection)
                reg.Vx[0xF] = (carryFlag == true) ? 1 : 0;
            }
            break;

            case 0xE: // See contained decoder
            {
                switch(parameters)
                {
                    case 0x9E: // Skip next if Key == Vx
                    {
                        u8 regVal = reg.Vx[opcodeType & 0xF];
                        if (inputState->isKeyDown[regVal] == true)
                        {
                            pcIncrement = 4;
                        }
                    }
                    break;
                    
                    case 0xA1: // Skip next if Key != Vx
                    {
                        u8 regVal = reg.Vx[opcodeType & 0xF];
                        if (inputState->isKeyDown[regVal] == false)
                        {
                            pcIncrement = 4;
                        }
                    }
                    break;

                    default:
                    {
                        setCPUState(asType(CPU_FLAGS::HALT));
                        pcIncrement = 0;
                    }
                    break;
                };                
            }
            break;

            case 0xF: // See contained decoder
            {
                switch(parameters)
                {
                    case 0x07: // Vx = Delay timer
                    {
                        reg.Vx[opcodeType & 0xF] = reg.delay;
                    }
                    break;

                    case 0x0A: // Vx = key pressed (blocking operation)
                    {
                        pcIncrement = 0;
                        
                        for (i32 loop = 0; loop < KeyMatrix; loop++)
                        {
                            if (inputState->keyTransitions[loop])
                            {
                                reg.Vx[opcodeType & 0xF] = static_cast<u8>(loop);
                                pcIncrement = 2;
                            }
                        }

                        // Early out of instruction stepping as no new keypress
                        // will be detected until the next simulation cycle
                        if (pcIncrement == 0)
                        {
                            stepValue = instructionCount;
                        }
                    }
                    break;
                
                    case 0x15: // Delay timer = Vx
                    {
                        reg.delay = reg.Vx[opcodeType & 0xF];
                    }
                    break;

                    case 0x18: // Sound timer = Vx
                    {
                        reg.sound = reg.Vx[opcodeType & 0xF];
                    }
                    break;

                    case 0x1E: // I += Vx
                    {
                        reg.I += reg.Vx[opcodeType & 0xF];
                    }
                    break;
                    
                    case 0x29: // I = Sprite[Vx]
                    {
                        u8 regVal = reg.Vx[opcodeType & 0xF];
                        reg.I = asType(MEMMAP::ROM_SPRITE) + (regVal * 5);
                    }
                    break;

                    case 0x30: // I = Extended Sprite[Vx]
                    {
                        u8 regVal = reg.Vx[opcodeType & 0xF];
                        reg.I = asType(MEMMAP::ROM_SPRITE) + (16 * 5) + (regVal * 10);
                    }
                    break;
                    
                    case 0x33: // I = 3 digit BCD of Vx
                    {
                        // NOTE(KN): There seem to be no examples of ROMs that make assumptions
                        // about the value of the I register *after* executing this opcode, but
                        // that's not to say there definitely isn't!
                        u8 regVal = reg.Vx[opcodeType & 0xF];
                        u8 *addr = mainMemory + reg.I;
                        addr[2] = regVal % 10;
                        regVal /= 10;
                        addr[1] = regVal % 10;
                        regVal /= 10;
                        addr[0] = regVal;
                    }
                    break;

                    case 0x55: // Store V0 .. Vx to I
                    {
                        u8 regLast = opcodeType & 0xF;
                        u8 *addr = mainMemory + reg.I;
                        for (u8 offset = 0; offset <= regLast; offset++)
                        {
                            *addr++ = reg.Vx[offset];
                        }

                        if (compatibleMode & asType(COMPATIBILITY_FLAGS::LOAD_STORE_INC))
                        {
                            reg.I += regLast + 1;
                        }                        
                    }
                    break;

                    case 0x65: // Load V0 .. Vx from I
                    {
                        u8 regLast = opcodeType & 0xF;
                        u8 *addr = mainMemory + reg.I;
                        for (u8 offset = 0; offset <= regLast; offset++)
                        {
                            reg.Vx[offset] = *addr++;
                        }

                        if (compatibleMode & asType(COMPATIBILITY_FLAGS::LOAD_STORE_INC))
                        {
                            reg.I += regLast + 1;
                        }                        
                    };
                    break;

                    case 0x75: // Store V0 .. Vx to backup
                    {
                        u8 regLast = opcodeType & 0xF;
                        for (u8 offset = 0; offset <= regLast; offset++)
                        {
                            reg.RPLx[offset] = reg.Vx[offset];
                        }
                    };
                    break;

                    case 0x85: // Restore V0 .. Vx from backup
                    {
                        u8 regLast = opcodeType & 0xF;
                        for (u8 offset = 0; offset <= regLast; offset++)
                        {
                            reg.Vx[offset] = reg.RPLx[offset];
                        }
                    };
                    break;

                    
                    default:
                    {
                        setCPUState(asType(CPU_FLAGS::HALT));
                        pcIncrement = 0;
                    }
                    break;
                }            
            }
            break;
        
            default:
            {
                setCPUState(asType(CPU_FLAGS::HALT));
                pcIncrement = 0;
            }
            break;
        };

        reg.PC += pcIncrement;
    }
}

void Chip8Interpreter::fillSoundBuffer(i32 sampleCount)
{
    if (isCPUState(asType(CPU_FLAGS::RESET_ACTIVE)))
        return;

    i16 *bufferWrite = sampleTarget;

    for (i32 loop = 0; loop < sampleCount; loop++)
    {
        if (!halfCycleCount)
        {
            // Don't produce sound when CPU is in debug state
            if (reg.sound && !isCPUState(asType(CPU_FLAGS::HALT)))
            {
                halfCycleAmplitude = halfCycleAmplitude ? -halfCycleAmplitude : toneAmplitude;
            }
            else
            {
                halfCycleAmplitude = 0;
            }

            halfCycleCount = samplesPerHalfCycle;
        }
        
        *bufferWrite++ = halfCycleAmplitude;
        *bufferWrite++ = halfCycleAmplitude;
        halfCycleCount--;
    }
}

u32 Chip8Interpreter::random()
{
    // Minimal 2-seed RNG from Marsaglia's MWC
    zState = 36969 * (zState & 65535) + (zState >> 16); 
    wState = 18000 * (wState & 65535) + (wState >> 16);  
    return ((zState << 16) + wState);
}

void Chip8Interpreter::loadROMSprites(u16 romBase)
{
    u8 *mappedByte = mainMemory + romBase;
    for (i32 loop = 0; loop < 80; loop++)
    {
        *mappedByte++ = spriteSet[loop];
    }

    for (i32 loop = 0; loop < 100; loop++)
    {
        *mappedByte++ = chip48SpriteSet[loop];
    }
}

void Chip8Interpreter::clearGraphicsMemory()
{
    for (i32 yLoop = 0; yLoop < displayHeight; yLoop++)
    {
        u8 *renderRow = reinterpret_cast<u8 *>(rgbaTarget) + (yLoop * displayPitch);
        u32 *pixelAddr = reinterpret_cast<u32 *>(renderRow);
        for (i32 xLoop = 0; xLoop < displayWidth; xLoop++)
        {
            *pixelAddr++ = 0x00000000;
        }
    }                        
}
