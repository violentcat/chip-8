#if !defined(CHIP8REFERENCE_H)
#define CHIP8REFERENCE_H

namespace Chip8
{
    const u32 StackSize = 24;
    const u32 KeyMatrix = 16;    

    enum class COMPATIBILITY_FLAGS : u8
    {
        LOAD_STORE_INC = 0x01,
        SHIFT_USE_VY = 0x02,
        WRAP_Y_AXIS = 0x04,
        WRAP_X_AXIS = 0x08
    };
    
    enum class CPU_FLAGS : u8
    {
        RESET_ACTIVE = 0x01,
        HALT = 0x02,
        STEP = 0x04
    };

    enum class MEMMAP : u16
    {
        RAM_START = 0x0000,
        ROM_SPRITE = 0x0020,
        PRG_START = 0x0200,
        ETI660_START = 0x0600,
        RAM_END = 0x0FFF        
    };

    u8 asType(COMPATIBILITY_FLAGS flags) {return static_cast<u8>(flags);}
    u8 asType(CPU_FLAGS flags) {return static_cast<u8>(flags);}
    u16 asType(MEMMAP address) {return static_cast<u16>(address);}
};

#endif
