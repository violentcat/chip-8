#if !defined(PLATFORMTYPES_H)
#define PLATFORMTYPES_H

#include <stdint.h>

#ifdef PLATFORM_DEBUG
#define platformAssert(Expression) if (!(Expression)) {*(int *)0 = 0;}
#else
#define platformAssert(Expression)
#endif

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float r32;
typedef double r64;

#endif
