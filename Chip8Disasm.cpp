
#include "Chip8Disasm.h"

static char HexDigits[] = "0123456789ABCDEF";

void Chip8Disassembler::loadString(const char *source, char *target)
{
    i8 index = 0;
    i8 targetLimit = sizeof(((Instruction *)0)->disasmText)-1;
    
    const char *retireChar = source;
    while(retireChar && index < targetLimit-1)
    {
        target[index++] = *retireChar++;
    }

    target[index] = 0;
}

void Chip8Disassembler::loadParamString(const char *source, char *target, u8 opcodeType, u8 parameters)
{
    i8 index = 0;
    i8 targetLimit = sizeof(((Instruction *)0)->disasmText)-1;
    
    const char *retireChar = source;
    while(retireChar && index < targetLimit-1)
    {
        if (*retireChar == '1')
        {
            target[index++] = HexDigits[opcodeType & 0xF];
        }
        else if (*retireChar == '2')
        {
            target[index++] = HexDigits[parameters >> 4];
        }
        else if (*retireChar == '3')
        {
            target[index++] = HexDigits[parameters & 0xF];
        }
        else
        {
            target[index++] = *retireChar;
        }
    
        retireChar++;
    }

    target[index] = 0;
}

void Chip8Disassembler::decodeMemoryBlock(void *memoryBlock, u16 start, Instruction *segment, i32 count, bool stdDraw)
{
    u16 currentPC = start;    
    u8 *mainMemory = reinterpret_cast<u8 *>(memoryBlock);

    for (i32 loop = 0; loop < count; loop++)
    {
        u8 *physicalLoc = mainMemory + currentPC; 
        u8 opcodeType = physicalLoc[0];
        u8 parameters = physicalLoc[1];    

        segment[loop].address = currentPC;
        segment[loop].rawBytes = (physicalLoc[0] << 8) + physicalLoc[1];
        char *instrBuf = segment[loop].disasmText;
        instrBuf[0] = 0;
        currentPC += 2;
        
        switch(opcodeType >> 4)
        {
            case 0x0: // See Contained Decoder
            {
                switch(parameters >> 4)
                {
                    case 0xC: // scroll display N lines down
                    {
                        loadParamString("SCD 3", instrBuf, opcodeType, parameters);
                    }
                    break;

                    default:
                    {
                        switch(parameters)
                        {
                            case 0xE0: // clear screen
                            {
                                loadString("CLS", instrBuf);
                            }
                            break;
                    
                            case 0xEE: // return from call
                            {
                                loadString("RET", instrBuf);
                            }
                            break;

                            case 0xFB: // scroll display 4 lines right
                            {
                                loadString("SCR", instrBuf);
                            }
                            break;                            
                            
                            case 0xFC: // scroll display 4 lines left
                            {
                                loadString("SCL", instrBuf);
                            }
                            break;                            

                            case 0xFD: // exit interpreter
                            {
                                loadString("HALT", instrBuf);
                            }
                            break;
                    
                            case 0xFE: // disable extended mode
                            {
                                loadString("DXM", instrBuf);
                            }
                            break;

                            case 0xFF: // enable extended mode
                            {
                                loadString("EXM", instrBuf);
                            }
                            break;
                    
                            default:
                            {
                                loadString("???", instrBuf);
                            }
                            break;
                        }                        
                    }
                    break;
                }                
            }
            break;
        
            case 0x1: // goto NNN
            {
                loadParamString("JP 0x0123", instrBuf, opcodeType, parameters);
            }
            break;

            case 0x2: // Call NNN
            {
                loadParamString("CALL 0x0123", instrBuf, opcodeType, parameters);
            }
            break;
        
            case 0x3: // Skip Next if (Vx == NN)
            {
                loadParamString("SE V1,23", instrBuf, opcodeType, parameters);                
            }
            break;

            case 0x4: // Skip Next if (Vx != NN)
            {
                loadParamString("SNE V1,23", instrBuf, opcodeType, parameters);                
            }
            break;

            case 0x5: // Skip Next if (Vx == Vy)
            {
                loadParamString("SE V1,V2", instrBuf, opcodeType, parameters);                
            }
            break;
        
            case 0x6: // Vx = NN
            {
                loadParamString("LD V1,23", instrBuf, opcodeType, parameters);
            }
            break;

            case 0x7: // Vx += NN
            {
                loadParamString("ADD V1,23", instrBuf, opcodeType, parameters);
            }
            break;

            case 0x8: // See Contained Decoder
            {
                switch(parameters & 0xF)
                {
                    case 0x0: // Vx = Vy
                    {
                        loadParamString("LD V1,V2", instrBuf, opcodeType, parameters);                        
                    }
                    break;

                    case 0x1: // Vx = Vx | Vy
                    {
                        loadParamString("OR V1,V2", instrBuf, opcodeType, parameters);                        
                    }
                    break;
                    
                    case 0x2: // Vx = Vx & Vy
                    {
                        loadParamString("AND V1,V2", instrBuf, opcodeType, parameters);                        
                    }
                    break;

                    case 0x3: // Vx = Vx ^ Vy
                    {
                        loadParamString("XOR V1,V2", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x4: // Vx += Vy (with carry)
                    {
                        loadParamString("ADD V1,V2", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x5: // Vx -= Vy (with !borrow)
                    {                        
                        loadParamString("SUB V1,V2", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x6: // Vx = Vy = Vy >> 1 (VF = pre-shift LSB of Vy)
                    {
                        loadParamString("SHR V1{,V2}", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x7: // Vx = Vy - Vx (with !borrow)
                    {
                        loadParamString("SUBN V1,V2", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0xE: // Vx = Vy = Vy << 1 (VF = pre-shift MSB of Vy)
                    {
                        loadParamString("SHL V1{,V2}", instrBuf, opcodeType, parameters);
                    }
                    break;
                    
                    default:
                    {
                        loadString("???", instrBuf);
                    }
                    break;
                }
            }
            break;

            case 0x9: // Skip Next if (Vx != Vy)
            {
                loadParamString("SNE V1,V2", instrBuf, opcodeType, parameters);
            }
            break;
            
            case 0xA: // I = NNN
            {
                loadParamString("LD I,123", instrBuf, opcodeType, parameters);
            }
            break;

            case 0xB: // goto NNN + V0
            {
                loadParamString("JP V0,123", instrBuf, opcodeType, parameters);
            }
            break;

            case 0xC: // Vx = RND & NN
            {
                loadParamString("RND V1,23", instrBuf, opcodeType, parameters);
            }
            break;

            case 0xD: // Draw (Vx,Vy,N)
            {
                if (stdDraw == false && !(parameters & 0xF))
                {
                    loadParamString("XDRW V1,V2", instrBuf, opcodeType, parameters);
                }
                else
                {
                    loadParamString("DRW V1,V2,3", instrBuf, opcodeType, parameters);
                }
            }
            break;

            case 0xE: // See contained decoder
            {
                switch(parameters)
                {
                    case 0x9E: // Skip next if Key == Vx
                    {
                        loadParamString("SKP V1", instrBuf, opcodeType, parameters);
                    }
                    break;
                    
                    case 0xA1: // Skip next if Key != Vx
                    {
                        loadParamString("SKNP V1", instrBuf, opcodeType, parameters);
                    }
                    break;

                    default:
                    {
                        loadString("???", instrBuf);
                    }
                    break;
                };                
            }
            break;

            case 0xF: // See contained decoder
            {
                switch(parameters)
                {
                    case 0x07: // Vx = Delay timer
                    {
                        loadParamString("LD V1,dT", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x0A: // Vx = key pressed (blocking operation)
                    {
                        loadParamString("LD V1,Key", instrBuf, opcodeType, parameters);
                    }
                    break;
                
                    case 0x15: // Delay timer = Vx
                    {
                        loadParamString("LD dT,V1", instrBuf, opcodeType, parameters);                        
                    }
                    break;

                    case 0x18: // Sound timer = Vx
                    {
                        loadParamString("LD sT,V1", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x1E: // I += Vx
                    {
                        loadParamString("ADD I,V1", instrBuf, opcodeType, parameters);
                    }
                    break;
                    
                    case 0x29: // I = Sprite[Vx]
                    {
                        loadParamString("LD I,Sprite[V1]", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x30: // I = Extended Sprite[Vx]
                    {
                        loadParamString("LD I,ExtSprite[V1]", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x33: // I = 3 digit BCD of Vx
                    {
                        loadParamString("LD I,BCD[V1]", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x55: // Store V0 .. Vx to I
                    {
                        loadParamString("LD [I],V0..V1", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x65: // Load V0 .. Vx from I
                    {
                        loadParamString("LD V0..V1,[I]", instrBuf, opcodeType, parameters);
                    };
                    break;

                    case 0x75: // Store V0 .. Vx to backup
                    {
                        loadParamString("LD R,V0..V1", instrBuf, opcodeType, parameters);
                    }
                    break;

                    case 0x85: // Restore V0 .. Vx from backup
                    {
                        loadParamString("LD V0..V1,R", instrBuf, opcodeType, parameters);
                    }
                    break;
                    
                    default:
                    {
                        loadString("???", instrBuf);
                    }
                    break;
                }            
            }
            break;
        
            default:
            {
                loadString("???", instrBuf);
            }
            break;
        };
    }    
}
