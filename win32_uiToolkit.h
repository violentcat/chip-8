#if !defined(WIN32_UITOOLKIT_H)
#define WIN32_UITOOLKIT_H

#include "Gdiplus.h"

class win32UIToolkit
{
public:

    struct TextStyle
    {
        Gdiplus::Font font;
        Gdiplus::Color colour;
        TextStyle(WCHAR *descriptor, i32 style, Gdiplus::Color rgbColour)
                : font(descriptor, 10, style, Gdiplus::UnitPixel)
            , colour(rgbColour) {}
    };

    struct LineStyle
    {
        Gdiplus::Color colour;
        LineStyle(Gdiplus::Color rgbColour)
            : colour(rgbColour) {}
    };

    Gdiplus::Bitmap *bitmapData = nullptr;
    Gdiplus::Graphics *drawObject = nullptr;
    
public:

    win32UIToolkit(BITMAPINFO *bitmapInfo, void *pixelData);
    ~win32UIToolkit();

    void beginDraw();

    void renderText(const char *stringA, TextStyle *style, i32 xPos, i32 yPos);
    void renderFilledRect(LineStyle *style, i32 xPos, i32 yPos, i32 width, i32 height);

    void endDraw();

private:

    const WCHAR* createCompatibleString(const char *);
};

#endif
