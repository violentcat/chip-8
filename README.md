# Chip-8

Implementation of a Chip-8 interpreter, principally as a first (simple) attempt at emulation programming. It supports both standard Chip-8 and Super-Chip (Chip-48) modes, including sound. It also implements a simple debugging interface with a program loader, disassembler and single step functions.

![Screenshot of GUI](images/debug.png)

## Usage
When first run the user will be prompted to select a ROM to load using a simple file selection UI. Once a ROM is selected and Run the debug UI will disappear and the Chip-8 display will fill the window. The user can toggle the debug UI on and off using the ESC key.

The original COSMAC VIP device that supported the Chip-8 language had 16 keys marked as:
```
1 | 2 | 3 | C
4 | 5 | 6 | D
7 | 8 | 9 | E
A | 0 | B | F
```

These are mapped to the following keys on a standard UK keyboard:
```
1 | 2 | 3 | 4
Q | W | E | R
A | S | D | F
Z | X | C | V
```

## Compatibility
Compatibility is generally good, and the interpreter allows for the 3 most popular differences of opinion.

- Load / Store Increment
- Shift Result in Vy
- X and Y Screen Wrapping

These compatibility modes are controlled with a registry of known problem ROMs, referenced by their MD5 hash. If a new ROM is identified that requires one of these flags to be set then it can be added to the registry.

Note: No ROMs are included in this repository however they are easy enough to find and download.

## Building
Simply run build.bat from within a Visual Studio command prompt.

## Future Enhancements
The following is a list of potential future enhancements.

- Simple post processing filter (box?) to blur the hard edges of the display
- Flicker reduction techniques (original device flickers due to the nature of the drawing routine)
- Replace existing GDI+ debug UI with custom graphics layer
- Move rendering to use final OpenGL / DirectX layer
- More advances filtering effects to emulate old CRT display

## Dependencies
- [STB](https://github.com/nothings/stb): Single file public domain libraries for C/C++
