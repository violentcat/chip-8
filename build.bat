@echo off

set release_mode=1
set compiler_flags=-nologo -fp:fast -Gm- -GR- -EHa- -Oi -FC -MP -GS-

if %release_mode% EQU 0 (
   set compiler_flags=%compiler_flags% -Od -MDd -Z7 -DPLATFORM_DEBUG=1
) else (
   set compiler_flags=%compiler_flags% -O2 -MT -Z7 -DPLATFORM_DEBUG=0
)

set compiler_includes=
set compiler_warnings=-W4 -WX -wd4100 -wd4065 -wd4189
set linker_flags=-incremental:no -opt:ref -debug
set libs=user32.lib gdi32.lib winmm.lib gdiplus.lib

set compiler_group=%compiler_includes% %compiler_flags% %compiler_warnings%
set linker_group=%libs% %linker_flags%

IF NOT EXIST .\build mkdir .\build
pushd .\build

del *.pdb > NUL 2>  NUL
cl %compiler_group% ..\Chip8.cpp -FmChip8.map /link %linker_group%

popd
