
#include "windows.h"
#include "PlatformTypes.h"
#include "win32_DisplayBuffer.cpp"
#include "win32_SoundBuffer.cpp"
#include "win32_DebugView.cpp"
#include "win32_uiToolkit.cpp"
#include "win32_FileSystem.cpp"
#include "Chip8Interpreter.cpp"
#include "Chip8Disasm.cpp"
#include "ProgramRegistry.cpp"

win32DisplayBuffer renderArea;

LRESULT CALLBACK WindowMessageHandler(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{    
    LRESULT handlerResult = 0;
    
    switch(message)
    {
        case WM_ACTIVATEAPP:
        {
        } break;

        case WM_DESTROY:            
        {
        } break;

        case WM_CLOSE:
        {
            PostQuitMessage(0);
        } break;

        case WM_SIZE:
        {
        } break;

        case WM_PAINT:
        {
            win32DisplayBuffer::Dimensions windowDim = renderArea.GetDeviceDimensions(windowHandle);
            
            PAINTSTRUCT paintInfo;
            HDC deviceContext = BeginPaint(windowHandle, &paintInfo);
            renderArea.updateDeviceFromBuffer(deviceContext, windowDim.width, windowDim.height);
            EndPaint(windowHandle, &paintInfo);
            
        } break;

        default:
        {
            handlerResult =  DefWindowProcA(windowHandle, message, wParam, lParam);
        } break;
    }
    
    return handlerResult;
}

u32 GetMD5AlignedSize(u32 baseSize)
{
    // Additional space for MD5 hash padding alignment
    u32 alignedSize = ((baseSize + 8) & 0xFFFFFFC0) + 64;
    return alignedSize;    
}

u32 LoadMD5ProgramMemory(char *programFilename, void* programMemory, u32 allocationSize,
                         ProgramRegistry::MD5Digest *md5Hash)
{
    u32 programSize = 0;

    if (programFilename)
    {    
        HANDLE fileHandle = CreateFileA(programFilename,
                                        GENERIC_READ,
                                        FILE_SHARE_READ,
                                        0,
                                        OPEN_EXISTING,
                                        0,
                                        0);

        if (fileHandle != INVALID_HANDLE_VALUE)
        {
            LARGE_INTEGER fileSize;
            if (GetFileSizeEx(fileHandle, &fileSize))
            {
                DWORD readCompleteSize;
                ReadFile(fileHandle, programMemory, allocationSize, &readCompleteSize, 0);

                programSize = readCompleteSize;
                u32 alignedProgram = GetMD5AlignedSize(programSize);
            
                ProgramRegistry::generateMD5(programMemory, programSize, alignedProgram, md5Hash);
            }
        }
    }

    return programSize;
}

void ProcessControllerState(Chip8Interpreter::ControllerState *inputState, bool *runningState, bool *debuggerEnabled)
{
    for (i32 loop = 0; loop < Chip8::KeyMatrix; loop++)
    {
        inputState->keyTransitions[loop] = 0;
    }

    MSG Message;
    while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        switch(Message.message)
        {
            case WM_QUIT:
            {
                *runningState = false;
            } break;

            case WM_KEYDOWN:
            case WM_KEYUP:
            {
                WPARAM keyCode = Message.wParam;
                bool isKeyDown = (Message.lParam & (1 << 31)) == 0 ? true : false;
                bool wasKeyDown = (Message.lParam & (1 << 30)) == 0 ? false : true;

                if (isKeyDown != wasKeyDown)
                {
                    switch(keyCode)
                    {
                        case 0x31:
                            inputState->isKeyDown[0x1] = isKeyDown;
                            inputState->keyTransitions[0x1]++;
                            break; // 1
                            
                        case 0x32:
                            inputState->isKeyDown[0x2] = isKeyDown;
                            inputState->keyTransitions[0x2]++;
                            break; // 2
                            
                        case 0x33:
                            inputState->isKeyDown[0x3] = isKeyDown;
                            inputState->keyTransitions[0x3]++;
                            break; // 3
                            
                        case 0x34:
                            inputState->isKeyDown[0xC] = isKeyDown;
                            inputState->keyTransitions[0xC]++;
                            break; // 4
                            
                        case 0x51:
                            inputState->isKeyDown[0x4] = isKeyDown;
                            inputState->keyTransitions[0x4]++;
                            break; // Q
                            
                        case 0x57:
                            inputState->isKeyDown[0x5] = isKeyDown;
                            inputState->keyTransitions[0x5]++;
                            break; // W
                            
                        case 0x45:
                            inputState->isKeyDown[0x6] = isKeyDown;
                            inputState->keyTransitions[0x6]++;
                            break; // E
                            
                        case 0x52:
                            inputState->isKeyDown[0xD] = isKeyDown;
                            inputState->keyTransitions[0xD]++;
                            break; // R
                            
                        case 0x41:
                            inputState->isKeyDown[0x7] = isKeyDown;
                            inputState->keyTransitions[0x7]++;
                            break; // A
                            
                        case 0x53:
                            inputState->isKeyDown[0x8] = isKeyDown;
                            inputState->keyTransitions[0x8]++;
                            break; // S
                            
                        case 0x44:
                            inputState->isKeyDown[0x9] = isKeyDown;
                            inputState->keyTransitions[0x9]++;
                            break; // D
                            
                        case 0x46:
                            inputState->isKeyDown[0xE] = isKeyDown;
                            inputState->keyTransitions[0xE]++;
                            break; // F
                            
                        case 0x5A:
                            inputState->isKeyDown[0xA] = isKeyDown;
                            inputState->keyTransitions[0xA]++;
                            break; // Z
                            
                        case 0x58:
                            inputState->isKeyDown[0x0] = isKeyDown;
                            inputState->keyTransitions[0x0]++;
                            break; // X
                            
                        case 0x43:
                            inputState->isKeyDown[0xB] = isKeyDown;
                            inputState->keyTransitions[0xB]++;
                            break; // C
                            
                        case 0x56:
                            inputState->isKeyDown[0xF] = isKeyDown;
                            inputState->keyTransitions[0xF]++;
                            break; // V

                        case VK_ESCAPE:
                        {
                            if (isKeyDown == false)
                            {
                                *debuggerEnabled = !*debuggerEnabled;
                            }
                        }
                        break;
                    };
                }
            }
            break;

            default:
            {
                TranslateMessage(&Message);
                DispatchMessageA(&Message);
            } break;
        }
    }
}

void ProcessDebugInputState(HWND windowHandle,
                            i32 windowWidth, i32 windowHeight,
                            i32 debugWidth, i32 debugHeight,
                            win32DebugView::IMController *input)
{
    POINT mousePos;
    GetCursorPos(&mousePos);
    ScreenToClient(windowHandle, &mousePos);

    input->mouseX = (mousePos.x * debugWidth) / windowWidth;
    input->mouseY = (mousePos.y * debugHeight) / windowHeight;
    
    input->leftButtonDown = GetKeyState(VK_LBUTTON) & 0x8000 ? true : false;
    input->middleButtonDown = GetKeyState(VK_MBUTTON) & 0x8000 ? true : false;
    input->rightButtonDown = GetKeyState(VK_RBUTTON) & 0x8000 ? true : false;
}

void ProgramLoop(HWND windowHandle)
{    
    bool programLoopRunning = true;
    bool debugOverlayActive = false;
    bool cursorVisible = true;

    if (timeBeginPeriod(1) != TIMERR_NOERROR)
    {
        // If can't raise the scheduler interrupt rate then no choice but to implement
        // simulation period delay as a busy loop
        // TODO(KN): Implement busy loop
        return;
    }
    
    // Initialise high resolution timing for frame interval
    LARGE_INTEGER performanceFreqResult;
    QueryPerformanceFrequency(&performanceFreqResult);
    r64 timerFrequency = static_cast<r64>(performanceFreqResult.QuadPart);
    
    // Initialise controller state
    Chip8Interpreter::ControllerState keyState = {};

    // Initialise memory space for mapped memory
    u32 addressibleMemory = asType(MEMMAP::RAM_END) + 1;
    void *cpuMemorySpace = HeapAlloc(GetProcessHeap(), 0, addressibleMemory);

    // Initialise memory space for ROM loader (based on maximum program size including
    // any alignment requirements for MD5 hash)
    u32 romLoadPage = asType(MEMMAP::RAM_END) - asType(MEMMAP::PRG_START);
    u32 romLoadAligned = GetMD5AlignedSize(romLoadPage);
    void *programMemory = HeapAlloc(GetProcessHeap(), 0, romLoadAligned);
    
    Chip8Interpreter executeCPU;
    Chip8Disassembler disassemblyCPU;
    executeCPU.mapMemorySpace(cpuMemorySpace, asType(MEMMAP::RAM_START), addressibleMemory);

    // Initialise sound generation with added 80ms buffer latency
    win32SoundBuffer soundHandler;
    soundHandler.createWaveformBuffer(windowHandle, 80);
    executeCPU.initSoundMemory(soundHandler.waveformBuffer, soundHandler.samplesPerSecond);
    
    // Initialise rendering layer (debug overlay target buffer)
    // TODO(KN): Get rid of magic numbers with some sort of layout definition
    win32DebugView debuggingView(&renderArea.displayBuffer.bitmapInfo, renderArea.displayBuffer.pixelData);
    debuggingView.windowData.screenPosition.x = renderArea.displayBuffer.width - 400;
    debuggingView.windowData.screenPosition.y = 10;

    // Initialise rendering layer (simulation target buffer)
    renderArea.createSimulationBuffer(executeCPU.display.screenWidth, executeCPU.display.screenHeight);
    executeCPU.initGraphicsMemory(renderArea.simBuffer.pixelData,
                                  renderArea.simBuffer.width, renderArea.simBuffer.height,
                                  renderArea.simBuffer.pitch);

    
    Chip8Disassembler::Instruction disasmBlock[16];

    bool displayROMLoad = false;
    char *currentROMFile = nullptr;

    while (programLoopRunning)
    {
        ProgramRegistry::MD5Digest md5Hash;
        u32 programSize = LoadMD5ProgramMemory(currentROMFile, programMemory, romLoadPage, &md5Hash);
        if (!programSize)
        {
            executeCPU.setCPUState(asType(CPU_FLAGS::HALT));
            executeCPU.invalidateMappedMemory();
        }

        // Set initial simulation parameters for loaded program
        u8 compatibilityFlags;
        i32 processorCyclesPerSecond;
        ProgramRegistry::configureSimulation(&md5Hash, &compatibilityFlags, &processorCyclesPerSecond);

        r64 simulationPeriod = 1000000.0 / 60.0;
        
        // Determine cycle timing for update + configured CPU cycles p/s
        r64 processorCyclesPerTick = (static_cast<double>(processorCyclesPerSecond) / 1000000.0) * simulationPeriod;    
        i32 instructionsPerTick = static_cast<i32>(processorCyclesPerTick + 0.5);

        // Determine sound generation scope per frame
        r64 waveformStatesPerTick = (static_cast<double>(soundHandler.samplesPerSecond) / 1000000.0) * simulationPeriod;
        i32 samplesPerTick = static_cast<i32>(waveformStatesPerTick + 0.5);
        i32 samplesCurrentCycle = samplesPerTick;
        
        executeCPU.setCompatibilityModes(compatibilityFlags);
        executeCPU.setCPUState(asType(CPU_FLAGS::RESET_ACTIVE));
        executeCPU.copyBlockToMapped(asType(MEMMAP::PRG_START), programMemory, programSize);
        executeCPU.resetCPUState(asType(CPU_FLAGS::RESET_ACTIVE));

        // Set up the exponential moving average for the frame time using the
        // 'standard' alpha value of 1/n+1 and we want to smooth over 1 seconds
        // worth of samples
        r64 averageSimTime = 0.0;
        r64 averageFrameTime = 0.0;
        r64 decayAlpha = 1.0 / static_cast<r64>(60 + 1);

        // Frame interval time is presented directly without smoothing
        r64 frameIntervalTime = 0.0;
        
        LARGE_INTEGER cycleInitTime;
        LARGE_INTEGER simCompleteTime;
        LARGE_INTEGER frameCompleteTime;
        LARGE_INTEGER frameSwapTime;
        QueryPerformanceCounter(&cycleInitTime);        
        
        bool simulationLoopRunning = true;
        while (simulationLoopRunning && programLoopRunning)
        {
            ProcessControllerState(&keyState, &programLoopRunning, &debugOverlayActive);
            executeCPU.simulationCycle(instructionsPerTick, &keyState);
            executeCPU.fillSoundBuffer(samplesCurrentCycle);
            QueryPerformanceCounter(&simCompleteTime);
        
            HDC deviceContext = GetDC(windowHandle);
        
            win32DisplayBuffer::Dimensions windowDim = renderArea.GetDeviceDimensions(windowHandle);
            if (debugOverlayActive == true || executeCPU.isCPUState(asType(CPU_FLAGS::HALT)))
            {
                if (cursorVisible == false)
                {
                    cursorVisible = true;
                    ShowCursor(TRUE);
                }
                
                // Update immediate mode mouse state (corrected for window size)
                ProcessDebugInputState(windowHandle,
                                       windowDim.width, windowDim.height,
                                       renderArea.displayBuffer.width, renderArea.displayBuffer.height,
                                       &debuggingView.windowData.input);
                win32DebugView::ButtonPressResult result = {win32DebugView::PRESS_NONE, nullptr};

                if (displayROMLoad)
                {
                    result = debuggingView.drawFileSelection(averageSimTime, averageFrameTime, frameIntervalTime);
                    switch(result.buttonPress)
                    {                        
                        case win32DebugView::PRESS_ROMSELECT:
                            displayROMLoad = false;
                            simulationLoopRunning = false;
                            currentROMFile = result.stringValue;
                            executeCPU.setCPUState(asType(CPU_FLAGS::HALT));
                            break;

                        case win32DebugView::PRESS_ROMCANCEL:
                            displayROMLoad = false;
                            break;
                    }
                }
                else
                {
                    // Display relevant debug UI based on whether the CPU is halted
                    if (executeCPU.isCPUState(asType(CPU_FLAGS::HALT)))
                    {
                        disassemblyCPU.decodeMemoryBlock(cpuMemorySpace, executeCPU.reg.PC, disasmBlock, 16, executeCPU.displayCompatibilityMode);
                    
                        result = debuggingView.drawOverlay(averageSimTime, averageFrameTime, frameIntervalTime, &executeCPU.reg, disasmBlock, 16);
                        switch(result.buttonPress)
                        {
                            case win32DebugView::PRESS_LOAD:
                                displayROMLoad = true;
                                break;
                            case win32DebugView::PRESS_RUNSTOP:
                                executeCPU.resetCPUState(asType(CPU_FLAGS::HALT));
                                break;
                            case win32DebugView::PRESS_STEP:
                                executeCPU.setCPUState(asType(CPU_FLAGS::STEP));
                                executeCPU.resetCPUState(asType(CPU_FLAGS::HALT));
                                break;
                        }
                    }
                    else
                    {
                        result = debuggingView.drawOverlay(averageSimTime, averageFrameTime, frameIntervalTime);
                        switch(result.buttonPress)
                        {
                            case win32DebugView::PRESS_LOAD:
                                displayROMLoad = true;
                                break;
                            case win32DebugView::PRESS_RUNSTOP:
                                executeCPU.setCPUState(asType(CPU_FLAGS::HALT));
                                break;
                        }                                       
                    }
                }
                
                i32 simWidth = debuggingView.windowData.screenPosition.x;
                i32 simHeight = (simWidth * renderArea.displayBuffer.height) / renderArea.displayBuffer.width;                                
                renderArea.renderSimulationToDisplay(executeCPU.displayCompatibilityMode, simWidth, simHeight);
            }
            else
            {
                if (cursorVisible == true)
                {
                    cursorVisible = false;
                    ShowCursor(FALSE);
                }
                
                renderArea.renderSimulationToDisplay(executeCPU.displayCompatibilityMode, renderArea.displayBuffer.width, renderArea.displayBuffer.height);
            }
            
            renderArea.updateDeviceFromBuffer(deviceContext, windowDim.width, windowDim.height);        
            ReleaseDC(windowHandle, deviceContext);

            soundHandler.updateDeviceFromWaveform(samplesCurrentCycle);
            samplesCurrentCycle = soundHandler.nextCycleSampleCount();
        
            // Measure elapsed time in microseconds for simulation cycle and total cycle (including debug ui)
            QueryPerformanceCounter(&frameCompleteTime);

            // Create moving averages for cycle times
            r64 simTimeElapsed = static_cast<r64>(simCompleteTime.QuadPart - cycleInitTime.QuadPart) * 1000000.0 / timerFrequency;        
            averageSimTime = (simTimeElapsed * decayAlpha) + (averageSimTime * (1.0 - decayAlpha));        
            r64 frameTimeElapsed = static_cast<r64>(frameCompleteTime.QuadPart - cycleInitTime.QuadPart) * 1000000.0 / timerFrequency;
            averageFrameTime = (frameTimeElapsed * decayAlpha) + (averageFrameTime * (1.0 - decayAlpha));        

            // .. then wait for the rest of the time to pass until the end of this cycle
            if (frameTimeElapsed < simulationPeriod)
            {
                DWORD waitTime = static_cast<DWORD>((simulationPeriod - frameTimeElapsed) / 1000.0);
                if (waitTime)
                {
                    Sleep(waitTime);
                }

                // TODO(KN): Do we need to spin here to consume any partial milliseconds
                // left over after sleep?
            }

            // Measure elapsed time in microseconds for complete frame interval
            QueryPerformanceCounter(&frameSwapTime);
            frameIntervalTime = static_cast<r64>(frameSwapTime.QuadPart - cycleInitTime.QuadPart) * 1000000.0 / timerFrequency;

            // Begin next interval
            QueryPerformanceCounter(&cycleInitTime);        
        }        
    }
}

int CALLBACK WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR commandLine, int showWindow)
{
    WNDCLASSEXA windowClass = {};

    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowMessageHandler;
    windowClass.hInstance = instance;
    windowClass.lpszClassName = "Chip8MainWindow";
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);

    renderArea.createDisplayBuffer(1244, 699);

    if (RegisterClassExA(&windowClass))
    {
        HWND windowHandle = CreateWindowExA(0,
                                            windowClass.lpszClassName,
                                            "Chip-8",
                                            WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                                            CW_USEDEFAULT,
                                            CW_USEDEFAULT,
                                            CW_USEDEFAULT,
                                            CW_USEDEFAULT,
                                            0,
                                            0,
                                            instance,
                                            0);
        
        if (windowHandle)
        {
            ProgramLoop(windowHandle);
        }
        else
        {
            // TODO(KN): Handle failure condition
        }
    }
    else
    {
        // TODO(KN): Handle failure condition
    }
    
    return 0;
}
