#if !defined(WIN32_FILESYSTEM_H)
#define WIN32_FILESYSTEM_H

class win32FileSystem
{
public:

    static const i32 FilePathLimit = 1024;
    #define STRING_DEF(s) ((StringDef *)s - 1)

    struct StringDef
    {
        i32 length;
        i32 capacity;
    };
    typedef char *FileString;

    void *filePathBuffer = nullptr;
    FileString nextFileString = nullptr;

    struct FilePath
    {
        FileString fileName;
        bool isFolder;
    };

public:

    static i32 fsCapacity(FileString str);
    static i32 fsLength(FileString str);
    static void fsSetLength(FileString str, i32 len);
    static void fsSetCurrentFolder(FileString str);
    static void fsCopy(FileString dst, FileString src);
    static void fsConcat(FileString dst, FileString src);
    static void fsRemoveEnd(FileString str, i32 count);
    static void fsSanitise(FileString dst, FileString src);

    static void fsFromRaw(FileString str, const char *raw, i32 position = 0);
    static void fsConcatFromRaw(FileString str, const char *raw);
    
    void createFileStringBuffer(i32 count);
    FileString reserveFileString();
    i32 getFileList(FileString folder, FilePath *fileList, i32 maxListSize, i32 offsetStart);
};

#endif
