#if !defined(WIN32_DEBUGVIEW_H)
#define WIN32_DEBUGVIEW_H

#include "Chip8Interpreter.h"
#include "Chip8Disasm.h"
#include "win32_FileSystem.h"

class win32UIToolkit;

class win32DebugView
{
public:

    enum ButtonPressCode
    {
        PRESS_LOAD,
        PRESS_RUNSTOP,
        PRESS_STEP,
        PRESS_ROMSELECT,
        PRESS_ROMCANCEL,
        PRESS_SCROLLINC,
        PRESS_SCROLLDEC,
        PRESS_NONE
    };

    struct ButtonPressResult
    {
        ButtonPressCode buttonPress;
        char *stringValue;
    };
    
    struct Vec2
    {
        i32 x;
        i32 y;
    };

    struct Vec4
    {
        i32 x;
        i32 y;
        i32 w;
        i32 h;
    };    

    struct IM_Id
    {
        void *item;
        void *parent;
        i32 index;        
    };
    
    struct IMResponse
    {
        bool hot;
        bool active;
        bool clicked;
    };
    
    struct IMState
    {
        IM_Id hotElement = IM_Id {0,0,0};
        IM_Id activeElement = IM_Id {0,0,0};
        bool activeSet = false;
    };

    struct IMController
    {
        i32 mouseX = 0;
        i32 mouseY = 0;
        bool leftButtonDown = false;
        bool middleButtonDown = false;
        bool rightButtonDown = false;
    };

    struct IMWindow
    {
        Vec4 screenPosition = Vec4 {0};
        IMController input;
        IMState state;
        win32UIToolkit *render = nullptr;
        win32FileSystem *fileSelect = nullptr;
    };
    
    IMWindow windowData;

    static const i32 FileSelectionListSize = 16;
    win32FileSystem::FileString currentFolder;
    win32FileSystem::FileString pathBuilder;
    win32FileSystem::FileString selectedFile;

    i32 filesInFolder = 0;
    i32 folderOffset = 0;
    bool updateFileList = false;
    
    
public:

    win32DebugView(BITMAPINFO *bitmap, void *pixelData);
    ~win32DebugView();

    
public:

    ButtonPressResult drawOverlay(r64 simTimeus, r64 frameTimeus, r64 frameIntervalus);
    
    ButtonPressResult drawOverlay(r64 simTimeus, r64 frameTimeus, r64 frameIntervalus,
                                Chip8Interpreter::RegisterContext *context,
                                Chip8Disassembler::Instruction *disasmBlock,
                                i32 instructionCount);

    ButtonPressResult drawFileSelection(r64 simTimeus, r64 frameTimeus, r64 frameIntervalus);

    static ButtonPressCode drawButtons(IMWindow *parent, bool running);
    static ButtonPressCode drawFileButtons(IMWindow *parent);
    static ButtonPressCode drawFileScroll(IMWindow *parent, i32 scrollIndex, i32 totalCount);
    static void drawRenderTimes(IMWindow *parent, r64 simTimeus, r64 frameTimeus, r64 frameIntervalus);
    static void drawCPUState(IMWindow *parent, Chip8Interpreter::RegisterContext *context);
    static void drawDisassembly(IMWindow *parent, Chip8Interpreter::RegisterContext *context,
                                Chip8Disassembler::Instruction *disasmBlock, i32 instructionCount);
    static i32 drawFileList(IMWindow *parent);
    
private:

    static IMResponse updateIMState(IMState *state, IMController *input, IM_Id id, Vec4 *rect);

    static bool drawButton(IMWindow *parent, IM_Id id, const char *label, Vec4 *rect);
    static bool drawWindow(IMWindow *parent, IM_Id id, const char *label, Vec4 *rect);
    static bool drawFile(IMWindow *parent, IM_Id id, const char *fileName, bool folder, Vec4 *rect);

    static Vec4 moveWindowToScreen(IMWindow *parent, i32 x, i32 y, i32 w, i32 h);
    static bool isMouseInside(IMController *input, Vec4 *rect);
    static bool isMatchingId(IM_Id *first, IM_Id *second);
};

#endif
