
#include "win32_DebugView.h"
#include "win32_uiToolkit.h"
#include "Gdiplus.h"

#define STB_SPRINTF_IMPLEMENTATION
#include "stb_sprintf.h"

namespace
{
    win32UIToolkit::TextStyle *defHeadText(nullptr);
    win32UIToolkit::TextStyle *defBodyText(nullptr);
    win32UIToolkit::TextStyle *defFileText(nullptr);
    win32UIToolkit::TextStyle *hotFileText(nullptr);
    win32UIToolkit::LineStyle *defFillLine(nullptr);
    win32UIToolkit::LineStyle *defTitleLine(nullptr);
    win32UIToolkit::LineStyle *hotTitleLine(nullptr);
    win32UIToolkit::LineStyle *activeTitleLine(nullptr);

    char formatString[256];        
    win32FileSystem::FilePath pathInfoTable[win32DebugView::FileSelectionListSize];
}

win32DebugView::win32DebugView(BITMAPINFO *bitmap, void *pixelData)
{
    windowData.render = new win32UIToolkit(bitmap, pixelData);
    windowData.fileSelect = new win32FileSystem;

    defHeadText = new win32UIToolkit::TextStyle(L"Verdana", Gdiplus::FontStyleBold, Gdiplus::Color(0,0,0));
    defBodyText = new win32UIToolkit::TextStyle(L"Lucida Console", Gdiplus::FontStyleRegular, Gdiplus::Color(0,0,0));
    defFileText = new win32UIToolkit::TextStyle(L"Lucida Console", Gdiplus::FontStyleRegular, Gdiplus::Color(0xFF,0xFF,0xFF));
    hotFileText = new win32UIToolkit::TextStyle(L"Lucida Console", Gdiplus::FontStyleBold, Gdiplus::Color(0x9F,0x7F,0x00));
    defFillLine = new win32UIToolkit::LineStyle(Gdiplus::Color(0x7F,0x7F,0x7F));
    defTitleLine = new win32UIToolkit::LineStyle(Gdiplus::Color(0x9F,0x7F,0x00));
    hotTitleLine = new win32UIToolkit::LineStyle(Gdiplus::Color(0xBF,0x9F,0x00));
    activeTitleLine = new win32UIToolkit::LineStyle(Gdiplus::Color(0x7F,0x7F,0x00));

    // Request additional file strings to hold working copies of path and
    // file name strings    
    i32 stringTableSize = FileSelectionListSize + 3;
    windowData.fileSelect->createFileStringBuffer(stringTableSize);
    pathBuilder = windowData.fileSelect->reserveFileString();
    selectedFile = windowData.fileSelect->reserveFileString();

    // Set initial state for current folder
    currentFolder = windowData.fileSelect->reserveFileString();
    win32FileSystem::fsSetCurrentFolder(currentFolder);
    updateFileList = true;

    // Populate path string table 
    for (i32 loop = 0; loop < FileSelectionListSize; loop++)
    {
        pathInfoTable[loop].fileName = windowData.fileSelect->reserveFileString();
    }
}
        
win32DebugView::~win32DebugView()
{
    // Don't bother cleaning up... this object is only destroyed
    // at closedown anyway and there are no synchronisation issues
}

win32DebugView::ButtonPressResult
win32DebugView::drawOverlay(r64 simTimeus, r64 frameTimeus, r64 frameIntervalus)
{
    windowData.render->beginDraw();
    
    drawRenderTimes(&windowData, simTimeus, frameTimeus, frameIntervalus);
    ButtonPressResult result = {drawButtons(&windowData, true), nullptr};
    
    windowData.render->endDraw();
    return result;
}

win32DebugView::ButtonPressResult
win32DebugView::drawOverlay(r64 simTimeus, r64 frameTimeus, r64 frameIntervalus,
                            Chip8Interpreter::RegisterContext *context,
                            Chip8Disassembler::Instruction *disasmBlock,
                            i32 instructionCount)
{
    windowData.render->beginDraw();

    drawRenderTimes(&windowData, simTimeus, frameTimeus, frameIntervalus);
    drawCPUState(&windowData, context);
    drawDisassembly(&windowData, context, disasmBlock, instructionCount);
    ButtonPressResult result = {drawButtons(&windowData, false), nullptr};

    windowData.render->endDraw();
    return result;
}

win32DebugView::ButtonPressResult
win32DebugView::drawFileSelection(r64 simTimeus, r64 frameTimeus, r64 frameIntervalus)
{
    if (updateFileList == true)
    {
        updateFileList = false;
        filesInFolder = windowData.fileSelect->getFileList(currentFolder, pathInfoTable, FileSelectionListSize, folderOffset);
    }
    
    windowData.render->beginDraw();

    drawRenderTimes(&windowData, simTimeus, frameTimeus, frameIntervalus);
    ButtonPressResult selectResult = {drawFileButtons(&windowData), nullptr};
    i32 fileIndex = drawFileList(&windowData);
    ButtonPressCode scrollResult = drawFileScroll(&windowData, folderOffset, filesInFolder);
    
    windowData.render->endDraw();

    if (scrollResult != PRESS_NONE)
    {
        if (scrollResult == PRESS_SCROLLINC && (folderOffset + FileSelectionListSize) < filesInFolder)
        {
            folderOffset++;
        }
        else if (scrollResult == PRESS_SCROLLDEC && folderOffset > 0)
        {
            folderOffset--;
        }
        updateFileList = true;
    }            
    
    if (selectResult.buttonPress == PRESS_NONE && fileIndex >= 0)
    {
        windowData.fileSelect->fsCopy(pathBuilder, currentFolder);
        windowData.fileSelect->fsConcatFromRaw(pathBuilder, "\\");
        windowData.fileSelect->fsConcat(pathBuilder, pathInfoTable[fileIndex].fileName);
        
        if (pathInfoTable[fileIndex].isFolder)
        {
            windowData.fileSelect->fsSanitise(currentFolder, pathBuilder);
            folderOffset = 0;
            updateFileList = true;
        }
        else
        {
            windowData.fileSelect->fsSanitise(selectedFile, pathBuilder);
            selectResult.buttonPress = PRESS_ROMSELECT;
            selectResult.stringValue = selectedFile;
        }
    }    
    
    return selectResult;
}

win32DebugView::ButtonPressCode
win32DebugView::drawButtons(IMWindow *parent, bool running)
{
    Vec4 buttonPosition = moveWindowToScreen(parent, 60, 90, 80, 16);
    IM_Id buttonId = {nullptr, &drawButtons, 0};

    ButtonPressCode result = PRESS_NONE;

    if (drawButton(parent, buttonId, "Load", &buttonPosition))
    {
        result = PRESS_LOAD;
    }

    buttonPosition.x += 100;
    buttonId.index++;
    if (drawButton(parent, buttonId, running ? "Halt" : "Run", &buttonPosition))
    {
        result = PRESS_RUNSTOP;
    }

    buttonPosition.x += 100;
    buttonId.index++;
    if (drawButton(parent, buttonId, "Step", &buttonPosition))
    {
        result = PRESS_STEP;
    }

    return result;
}

win32DebugView::ButtonPressCode win32DebugView::drawFileButtons(IMWindow *parent)
{
    Vec4 buttonPosition = moveWindowToScreen(parent, 60, 90, 80, 16);
    IM_Id buttonId = {nullptr, &drawFileButtons, 0};

    ButtonPressCode result = PRESS_NONE;

    if (drawButton(parent, buttonId, "Cancel", &buttonPosition))
    {
        result = PRESS_ROMCANCEL;
    }

    return result;
}

win32DebugView::ButtonPressCode
win32DebugView::drawFileScroll(IMWindow *parent, i32 scrollIndex, i32 totalCount)
{
    Vec4 scrollPosition = moveWindowToScreen(parent, 360, 120, 10, 16);
    IM_Id buttonId = {nullptr, &drawFileScroll, 0};

    ButtonPressCode result = PRESS_NONE;

    if (drawButton(parent, buttonId, " ", &scrollPosition))
    {
        result = PRESS_SCROLLDEC;
    }

    scrollPosition.y += 16;
    i32 scrollBarTop = scrollPosition.y;
    
    i32 scrollBarHeight = (16*FileSelectionListSize)-(2*scrollPosition.h);
    i32 scrollHandleHeight = scrollBarHeight;

    r32 scrollBarScaling = (r32)FileSelectionListSize / (r32)totalCount;
    if (scrollBarScaling < 1.0f)
    {
        scrollHandleHeight = (i32)(scrollBarScaling * (r32)scrollBarHeight);

        r32 scrollOffsetScaling = (r32)scrollIndex / (r32)totalCount;
        i32 scrollBarOffset = (i32)(scrollOffsetScaling * (r32)scrollBarHeight);
        scrollPosition.y += scrollBarOffset;
    }
    
    scrollPosition.h = scrollHandleHeight;
    parent->render->renderFilledRect(defFillLine, scrollPosition.x, scrollPosition.y,
                                     scrollPosition.w, scrollPosition.h);

    scrollPosition.y = scrollBarTop + scrollBarHeight;
    scrollPosition.h = 16;

    buttonId.index++;
    if (drawButton(parent, buttonId, " ", &scrollPosition))
    {
        result = PRESS_SCROLLINC;
    }
    
    return result;
}

void win32DebugView::drawRenderTimes(IMWindow *parent, r64 simTimeus, r64 frameTimeus, r64 frameIntervalus)
{
    Vec4 framePosition = moveWindowToScreen(parent, 40, 8, 320, 68);    
    drawWindow(parent, {&drawRenderTimes}, "Renderer", &framePosition);

    framePosition.x += 5;
    framePosition.y += 22;
    
    stbsp_snprintf(formatString, 256, "Sim time(ms) : %.02f", simTimeus / 1000.0);
    parent->render->renderText(formatString, defBodyText, framePosition.x, framePosition.y);

    stbsp_snprintf(formatString, 256, "Frame time(ms) : %.02f", frameTimeus / 1000.0);
    parent->render->renderText(formatString, defBodyText, framePosition.x, framePosition.y+15);    

    stbsp_snprintf(formatString, 256, "Frame Interval(ms) : %.02f", frameIntervalus / 1000.0);
    parent->render->renderText(formatString, defBodyText, framePosition.x, framePosition.y+30);    
}

void win32DebugView::drawCPUState(IMWindow *parent, Chip8Interpreter::RegisterContext *context)
{
    Vec4 framePosition = moveWindowToScreen(parent, 40, 120, 320, 100);
    drawWindow(parent, {&drawCPUState}, "CPU State", &framePosition);

    framePosition.x += 5;
    framePosition.y += 22;        

    stbsp_snprintf(formatString, 256, "V0:%02X V1:%02X V2:%02X V3:%02X V4:%02X V5:%02X V6:%02X V7:%02X",
                   context->Vx[0], context->Vx[1], context->Vx[2], context->Vx[3],
                   context->Vx[4], context->Vx[5], context->Vx[6], context->Vx[7]);
    parent->render->renderText(formatString, defBodyText, framePosition.x, framePosition.y);
    
    stbsp_snprintf(formatString, 256, "V8:%02X V9:%02X VA:%02X VB:%02X VC:%02X VD:%02X VE:%02X VF:%02X",
                   context->Vx[8], context->Vx[9], context->Vx[10], context->Vx[11],
                   context->Vx[12], context->Vx[13], context->Vx[14], context->Vx[15]);
    parent->render->renderText(formatString, defBodyText, framePosition.x, framePosition.y+20);
    
    stbsp_snprintf(formatString, 256, "I:0x%04X PC:0x%04X Stack:%02X dT:%02X sT:%02X",
                   context->I, context->PC, context->SP, context->delay, context->sound);
    parent->render->renderText(formatString, defBodyText, framePosition.x, framePosition.y+60);    
}

void win32DebugView::drawDisassembly(IMWindow *parent, Chip8Interpreter::RegisterContext *context,
                                     Chip8Disassembler::Instruction *disasmBlock, i32 instructionCount)
{
    Vec4 framePosition = moveWindowToScreen(parent, 40, 260, 320, 340);
    drawWindow(parent, {&drawDisassembly}, "Disassembly", &framePosition);

    for (i32 loop = 0; loop < instructionCount; loop++)
    {
        bool curInstr = (disasmBlock[loop].address == context->PC) ? true : false;

        framePosition.y += 20;
        stbsp_snprintf(formatString, 256, "%s 0x%04X   %04X    %s", curInstr ? "->" : "  ", 
                       disasmBlock[loop].address, disasmBlock[loop].rawBytes, disasmBlock[loop].disasmText);        
        parent->render->renderText(formatString, defBodyText, framePosition.x, framePosition.y);
    }
}

i32 win32DebugView::drawFileList(IMWindow *parent)
{
    i32 selectedFile = -1;
    Vec4 textPosition = moveWindowToScreen(parent, 40, 120, 320, 10);    
    IM_Id selectionId = {nullptr, &drawFileList, 0};
    
    for (i32 loop = 0; loop < FileSelectionListSize; loop++)
    {
        if (win32FileSystem::fsLength(pathInfoTable[loop].fileName) == 0)
            break;
        
        if (drawFile(parent, selectionId,
                     pathInfoTable[loop].fileName, pathInfoTable[loop].isFolder,
                     &textPosition))
        {
            selectedFile = selectionId.index;
        }
        
        textPosition.y += 16;
        selectionId.index++;
    }

    return selectedFile;
}

win32DebugView::IMResponse win32DebugView::updateIMState(IMState *state, IMController *input, IM_Id id, Vec4 *rect)
{
    IMResponse returnAs = {false, false, false};
    returnAs.hot = isMatchingId(&state->hotElement, &id);
    returnAs.active = isMatchingId(&state->activeElement, &id);

    // Encode active state - mouse interacting with element
    if (returnAs.active)
    {
        if (!input->leftButtonDown)
        {
            if (returnAs.hot)
            {
                returnAs.clicked = true;
            }

            state->activeElement = {0};
            state->activeSet = false;
            returnAs.active = false;
        }
    }
    else
    {
        if (returnAs.hot && input->leftButtonDown && !state->activeSet)
        {
            state->activeElement = id;
            state->activeSet = true;
            returnAs.active = true;
        }        
    }

    // Encode hot state - mouse 'over' element
    if (isMouseInside(input, rect))
    {
        if (!state->activeSet)
        {
            state->hotElement = id;
            returnAs.hot = true;
        }            
    }
    else
    {
        if (returnAs.hot)
        {
            state->hotElement = {0};
            returnAs.hot = false;
        }
    }    

    return returnAs;
}

bool win32DebugView::drawButton(IMWindow *parent, IM_Id id, const char *label, Vec4 *rect)
{
    Vec4 buttonData = *rect;
    buttonData.h = 16;

    IMResponse result = updateIMState(&parent->state, &parent->input, id, &buttonData);
    win32UIToolkit::LineStyle *style = result.hot ? hotTitleLine : defTitleLine;
    style = result.active ? activeTitleLine : style;

    parent->render->renderFilledRect(style, buttonData.x, buttonData.y, buttonData.w, buttonData.h);

    buttonData.x += result.active ? 4 : 5;
    buttonData.y += result.active ? 1 : 2;    
    parent->render->renderText(label, defHeadText, buttonData.x, buttonData.y);

    return result.clicked;
}

bool win32DebugView::drawWindow(IMWindow *parent, IM_Id id, const char *label, Vec4 *rect)
{
    Vec4 titleData = *rect;
    titleData.h = 16;

    IMResponse result = updateIMState(&parent->state, &parent->input, id, rect);
    win32UIToolkit::LineStyle *style = result.hot ? hotTitleLine : defTitleLine;
    
    parent->render->renderFilledRect(defFillLine, rect->x, rect->y, rect->w, rect->h);
    parent->render->renderFilledRect(style, titleData.x, titleData.y, titleData.w, titleData.h);
    
    titleData.x += 5;
    titleData.y += 2;
    parent->render->renderText(label, defHeadText, titleData.x, titleData.y);

    return false;
}

bool win32DebugView::drawFile(IMWindow *parent, IM_Id id, const char *fileName, bool folder, Vec4 *rect)    
{
    IMResponse result = updateIMState(&parent->state, &parent->input, id, rect);
    win32UIToolkit::TextStyle *style = result.hot ? hotFileText : defFileText;

    stbsp_snprintf(formatString, 256, "%s %s",  folder ? "<DIR>" : "     ", fileName);
    parent->render->renderText(formatString, style, rect->x, rect->y);

    return result.clicked;    
}

inline win32DebugView::Vec4 win32DebugView::moveWindowToScreen(IMWindow *parent, i32 x, i32 y, i32 w, i32 h)
{
    Vec4 window = {x, y, w, h};
    window.x += parent->screenPosition.x;
    window.y += parent->screenPosition.y;
    return window;
}

bool win32DebugView::isMouseInside(IMController *input, Vec4 *rect)
{
    bool inside = false;
    
    if (input->mouseX > rect->x &&
        input->mouseX < (rect->x + rect->w) &&
        input->mouseY > rect->y &&
        input->mouseY < (rect->y + rect->h))
    {       
        inside = true;
    }
        
    return inside;
}

bool win32DebugView::isMatchingId(IM_Id *first, IM_Id *second)
{
    bool match = false;
    
    if (first->item == second->item &&
        first->parent == second->parent &&
        first->index == second->index)
    {
        match = true;
    }

    return match;
}

