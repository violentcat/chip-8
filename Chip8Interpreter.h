#if !defined(CHIP8INTERPRETER_H)
#define CHIP8INTERPRETER_H

#include "Chip8Reference.h"

class Chip8Interpreter
{
public:

    struct RegisterContext
    {
        u8 Vx[16];
        u8 RPLx[16];
        u8 delay;
        u8 sound;
        u16 I;
        u16 PC;
        u8 SP;
    };

    RegisterContext reg;
    u16 stackMem[Chip8::StackSize];

    struct ControllerState
    {
        bool isKeyDown[Chip8::KeyMatrix];
        u32 keyTransitions[Chip8::KeyMatrix];
    };

    struct DisplayArea
    {
        // Support SuperChip variant with double resolution
        i32 screenWidth = 128;
        i32 screenHeight = 64;
    };

    DisplayArea display;
    bool displayCompatibilityMode = true;

    i32 toneFrequencyHz = 262;
    i16 toneAmplitude = 1000;
    

public:

    void setCompatibilityModes(u8 flags);
    
    bool isCPUState(u8 flags);
    void setCPUState(u8 flags);
    void resetCPUState(u8 flags);

    void mapMemorySpace(void *memoryBlock, u16 start, i32 length);    
    void initGraphicsMemory(void *pixelData, i32 width, i32 height, i32 pitch);
    void initSoundMemory(void *waveformBuffer, i32 samplesPerSecond);

    void invalidateMappedMemory();
    void copyBlockToMapped(u16 addressStart, void *block, i32 blockLength);
    void copyBlockFromMapped(u16 addressStart, void *block, i32 blockLength);
    
    void simulationCycle(i32 instructionCount, ControllerState *inputState);
    void fillSoundBuffer(i32 sampleCount);
    

private:

    // CPU
    u8 currentCPUState = 0;
    u8 *mainMemory = nullptr;
    i32 mappedMemorySize;
    u32 firstAddress;
    u8 compatibleMode = 0;

    // Graphics
    u32 *rgbaTarget = nullptr;
    i32 displayWidth;
    i32 displayHeight;
    i32 displayPitch;

    // Sound
    i16 *sampleTarget = nullptr;
    i32 sampleHz;
    i32 samplesPerHalfCycle;
    i16 halfCycleAmplitude = 0;
    i32 halfCycleCount = 0;

    // RNG states
    u32 zState = 1234;
    u32 wState = 42;
 
    
private:

    u32 random();        
    void loadROMSprites(u16 romBase);
    void clearGraphicsMemory();
};

#endif
