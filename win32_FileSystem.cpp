
#include "win32_FileSystem.h"

i32 win32FileSystem::fsCapacity(FileString str)
{
    return STRING_DEF(str)->capacity;
}

i32 win32FileSystem::fsLength(FileString str)
{
    return STRING_DEF(str)->length;
}

void win32FileSystem::fsSetLength(FileString str, i32 len)
{
    STRING_DEF(str)->length = len;
    str[len] = 0;
}

void win32FileSystem::fsSetCurrentFolder(FileString str)
{
    DWORD length = GetCurrentDirectoryA(fsCapacity(str), str);
    fsSetLength(str, length);
}

void win32FileSystem::fsCopy(FileString dst, FileString src)
{
    i32 srcLength = fsLength(src);
    char *srcBuffer = src;
    char *dstBuffer = dst;
    
    for (i32 loop = 0; loop < srcLength; loop++)
    {
        *dstBuffer++ = *srcBuffer++;
    }

    fsSetLength(dst, srcLength);
}

void win32FileSystem::fsConcat(FileString dst, FileString src)
{
    i32 currLen = fsLength(dst);

    i32 srcLength = fsLength(src);
    char *srcBuffer = src;
    char *dstBuffer = &dst[currLen];
    
    for (i32 loop = 0; loop < srcLength; loop++)
    {
        *dstBuffer++ = *srcBuffer++;
    }

    fsSetLength(dst, currLen+srcLength);    
}

void win32FileSystem::fsRemoveEnd(FileString str, i32 count)
{
    i32 newLength = fsLength(str) - count;
    if (newLength < 0)
    {
        newLength = 0;
    }

    fsSetLength(str, newLength);
}

void win32FileSystem::fsSanitise(FileString dst, FileString src)
{
    DWORD finalLength = GetFullPathNameA(src, fsCapacity(dst), dst, NULL);
    fsSetLength(dst, finalLength);
}

void win32FileSystem::fsFromRaw(FileString str, const char *raw, i32 position)
{
    i32 currLen = position;

    char *dstBuffer = &str[currLen];
    const char *srcBuffer = raw;

    if (srcBuffer)
    {
        while(*srcBuffer && (currLen < FilePathLimit))
        {
            *dstBuffer++ = *srcBuffer++;
            currLen++;
        }

        fsSetLength(str, currLen);
    }
}

void win32FileSystem::fsConcatFromRaw(FileString str, const char *raw)
{
    i32 currLen = fsLength(str);
    fsFromRaw(str, raw, currLen);
}

void win32FileSystem::createFileStringBuffer(i32 count)
{
    if (filePathBuffer)
    {
        HeapFree(GetProcessHeap(), 0, filePathBuffer);
    }

    i32 sizePerStringEntry = sizeof(StringDef) + FilePathLimit + 1;    
    i32 bufferSize = sizePerStringEntry * count;
    
    filePathBuffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, bufferSize);
    nextFileString = reinterpret_cast<FileString>(filePathBuffer);
    nextFileString += sizeof(StringDef);

    FileString stringIter = nextFileString;
    for (i32 loop = 0; loop < count; loop++)
    {
        StringDef *currentDef = STRING_DEF(stringIter);
        currentDef->capacity = FilePathLimit;
        stringIter += (currentDef->capacity + 1);
    }
}

win32FileSystem::FileString win32FileSystem::reserveFileString()
{
    FileString returnValue = nextFileString;

    StringDef *currentDef = STRING_DEF(nextFileString);
    nextFileString += (currentDef->capacity + 1);
    return returnValue;
}

i32 win32FileSystem::getFileList(FileString folder, FilePath *fileList, i32 maxListSize, i32 offsetStart)
{
    fsConcatFromRaw(folder, "\\*");
    i32 index = 0;
    i32 skipCount = 0;
    i32 filesCount = 0;
    
    WIN32_FIND_DATA findFileData;
    HANDLE fileIterator = FindFirstFileA(folder, &findFileData);
    if (fileIterator != INVALID_HANDLE_VALUE)
    {
        do
        {
            if (++skipCount > offsetStart)
            {
                if (index < maxListSize)
                {
                    fsFromRaw(fileList[index].fileName, findFileData.cFileName);
                    fileList[index].isFolder = (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ? true : false;
                }
                index++;
            }
            filesCount++;
        }
        while (FindNextFileA(fileIterator, &findFileData) != 0);
    }        

    FindClose(fileIterator);

    // Ensure any unused file path containers are empty
    while (index < maxListSize)
    {
        fsSetLength(fileList[index].fileName, 0);
        index++;
    }

    fsRemoveEnd(folder, 2);
    return filesCount;
}


