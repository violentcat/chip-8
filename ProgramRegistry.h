#if !defined(PROGRAMREGISTRY_H)
#define PROGRAMREGISTRY_H

class ProgramRegistry
{
public:
    
    struct MD5Digest
    {
        u8 block[16];
    };

    struct ProgramDescriptor
    {
        i32 cyclesPerSecond;
        MD5Digest hash;
        char *name;
        char *author;
        char *date;
        u8 flags;
    };

    static void generateMD5(void *programData, u32 programSize, u32 alignedSize, MD5Digest *hash);
    static void configureSimulation(MD5Digest *hash, u8 *flags, i32 *cyclesPerSecond);
};

#endif
