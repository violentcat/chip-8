#if !defined(CHIP8DISASM_H)
#define CHIP8DISASM_H

class Chip8Disassembler
{
public:

    struct Instruction
    {
        u16 address;
        u16 rawBytes;
        char disasmText[16];
    };
    
    
public:

    void loadString(const char *source, char *target);
    void loadParamString(const char *source, char *target, u8 opcodeType, u8 parameters);

    void decodeMemoryBlock(void *memoryBlock, u16 start, Instruction *segment, i32 count, bool stdDraw);
};

#endif
