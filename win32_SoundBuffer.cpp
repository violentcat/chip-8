
#include "win32_SoundBuffer.h"
#include <dsound.h>

//******************
// Direct Sound Imports
typedef HRESULT WINAPI DirectSoundCreate_func(LPCGUID, LPDIRECTSOUND *, LPUNKNOWN);
static HRESULT WINAPI stub_DirectSoundCreate(LPCGUID, LPDIRECTSOUND *, LPUNKNOWN) {return E_FAIL;}
static DirectSoundCreate_func *DirectSoundCreate_Dynamic = stub_DirectSoundCreate;
//******************

void win32SoundBuffer::createWaveformBuffer(HWND windowHandle, i32 latencyms)
{
    initDirectSound(windowHandle, latencyms);

    if (waveformBuffer)
    {
        HeapFree(GetProcessHeap(), 0, waveformBuffer);
    }
    
    i32 waveformSize = samplesPerSecond * bytesPerSample;
    waveformBuffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, waveformSize);
}

void win32SoundBuffer::updateDeviceFromWaveform(i32 sampleCount)
{
    LPDIRECTSOUNDBUFFER activeBuffer = reinterpret_cast<LPDIRECTSOUNDBUFFER>(secondaryBuffer);
    platformAssert(activeBuffer);
    
    i32 byteSizeToLock = sampleCount * bytesPerSample;
        
    void *firstFill;
    DWORD firstFillSize;
    void *secondFill;
    DWORD secondFillSize;    

    HRESULT result;
    result = activeBuffer->Lock(activeWriteCursor, byteSizeToLock,
                                &firstFill, &firstFillSize, &secondFill, &secondFillSize, 0);

    // NOTE(KN): Not sure if this condition can still happen but it doesn't
    // cost much to have the restore functionality here just in case
    if (result == DSERR_BUFFERLOST)
    {
        activeBuffer->Restore();
        result = activeBuffer->Lock(activeWriteCursor, byteSizeToLock,
                                    &firstFill, &firstFillSize, &secondFill, &secondFillSize, 0);        
    }

    if (SUCCEEDED(result))
    {
        i16 *sourceSample = reinterpret_cast<i16 *>(waveformBuffer);
        i16 *targetSample;

        i32 firstFillSampleCount = firstFillSize / bytesPerSample;
        targetSample = reinterpret_cast<i16 *>(firstFill);
        for (i32 index = 0; index < firstFillSampleCount; index++)
        {
            *targetSample++ = *sourceSample++;
            *targetSample++ = *sourceSample++;            
        }

        i32 secondFillSampleCount = secondFillSize / bytesPerSample;
        targetSample = reinterpret_cast<i16 *>(secondFill);
        for (i32 index = 0; index < secondFillSampleCount; index++)
        {
            *targetSample++ = *sourceSample++;
            *targetSample++ = *sourceSample++;            
        }
        
        activeWriteCursor += byteSizeToLock;
        if (activeWriteCursor >= activeBufferSizeBytes)
        {
            activeWriteCursor -= activeBufferSizeBytes;
        }
            
        activeBuffer->Unlock(firstFill, firstFillSize, secondFill, secondFillSize);
    }
        
    if (!playbackActive)
    {        
        activeBuffer->Play(0, 0, DSBPLAY_LOOPING);
        playbackActive = true;
    }
}

i32 win32SoundBuffer::nextCycleSampleCount()
{
    LPDIRECTSOUNDBUFFER activeBuffer = reinterpret_cast<LPDIRECTSOUNDBUFFER>(secondaryBuffer);
    platformAssert(activeBuffer);

    DWORD playCursor;
    activeBuffer->GetCurrentPosition(&playCursor, nullptr);

    // NOTE(KN): Assumes invariant that difference is always less than buffersize/2
    // otherwise there is no defined answer taking wrap around into account
    i32 cursorError = playCursor - predictedReadCursor;
    if (cursorError >= (activeBufferSizeBytes / 2))
    {
        cursorError -= activeBufferSizeBytes;
    }
    else if (cursorError <= (-activeBufferSizeBytes / 2))
    {
        cursorError += activeBufferSizeBytes;
    }

    // Only apply half the cursor error to 'smooth' the feedback loop
    predictedBytesPerCycle += (cursorError / 2);
    if (predictedBytesPerCycle < 0)
    {
        predictedBytesPerCycle = 0;
    }

    i32 nextPredictedLoad = predictedBytesPerCycle;

    // Play cursor tracking (using predicted bytes per cycle for this device)
    predictedReadCursor = playCursor + predictedBytesPerCycle;
    if (predictedReadCursor >= activeBufferSizeBytes)
    {
        predictedReadCursor -= activeBufferSizeBytes;
    }

    // Write cursor tracking to maintain write ahead margin based on latency
    // configured at setup time. Includes complete reset of write cursor if
    // write ahead is completely out of spec (i.e. tracking must have failed)
    i32 currentWriteAhead = activeWriteCursor - playCursor;
    if (currentWriteAhead < 0)
    {
        currentWriteAhead += activeBufferSizeBytes;
    }

    if (currentWriteAhead > (activeBufferSizeBytes / 2))
    {
        activeWriteCursor = playCursor + writeAheadBytes;
        if (activeWriteCursor >= activeBufferSizeBytes)
        {
            activeWriteCursor -= activeBufferSizeBytes;
        }
    }
    else
    {
        i32 padToWriteAhead = writeAheadBytes - currentWriteAhead;
        nextPredictedLoad += padToWriteAhead;
        if (nextPredictedLoad < 0)
        {
            nextPredictedLoad = 0;
        }
    }

    i32 predictedSampleCount = nextPredictedLoad / bytesPerSample;    
    return predictedSampleCount;
}

void win32SoundBuffer::initDirectSound(HWND windowHandle, i32 latencyms)
{
    HMODULE dSoundExternal = LoadLibraryA("dsound.dll");
    if (dSoundExternal)
    {
        DirectSoundCreate_Dynamic = reinterpret_cast<DirectSoundCreate_func *>(GetProcAddress(dSoundExternal, "DirectSoundCreate"));
        if (!DirectSoundCreate_Dynamic)
        {
            DirectSoundCreate_Dynamic = stub_DirectSoundCreate;
        }
    }

    LPDIRECTSOUND controlInterface;
    if (SUCCEEDED(DirectSoundCreate_Dynamic(NULL, &controlInterface, nullptr)))
    {
        if (SUCCEEDED(controlInterface->SetCooperativeLevel(windowHandle, DSSCL_PRIORITY)))
        {
            // Create primary buffer (this is a mapping to the device and is not a buffer that
            // *really* exists for us to use other than initial configuration)
            DSBUFFERDESC bufferSettings = {};
            bufferSettings.dwSize = sizeof(DSBUFFERDESC);
            bufferSettings.dwFlags = DSBCAPS_PRIMARYBUFFER;

            LPDIRECTSOUNDBUFFER primaryBuffer;
            controlInterface->CreateSoundBuffer(&bufferSettings, &primaryBuffer, nullptr);

            WAVEFORMATEX waveformSettings = {};
            waveformSettings.wFormatTag = WAVE_FORMAT_PCM;
            waveformSettings.nChannels = 2;
            waveformSettings.nSamplesPerSec = samplesPerSecond;
            waveformSettings.nBlockAlign = static_cast<WORD>((channelCount * sampleSizeBits) / 8);
            waveformSettings.nAvgBytesPerSec = waveformSettings.nSamplesPerSec * waveformSettings.nBlockAlign;
            waveformSettings.wBitsPerSample = 16;
            waveformSettings.cbSize = 0;
            
            if (SUCCEEDED(primaryBuffer->SetFormat(&waveformSettings)))
            {
                // Create secondard buffer, which is the one we actually write into
                DSBUFFERDESC secondarySettings = {};
                secondarySettings.dwSize = sizeof(DSBUFFERDESC);
                secondarySettings.dwFlags = DSBCAPS_GETCURRENTPOSITION2;
                secondarySettings.dwBufferBytes = circularBufferSeconds * samplesPerSecond * waveformSettings.nBlockAlign;
                secondarySettings.lpwfxFormat = &waveformSettings;
                secondarySettings.guid3DAlgorithm = GUID_NULL;

                LPDIRECTSOUNDBUFFER tempBuffer;
                if (SUCCEEDED(controlInterface->CreateSoundBuffer(&secondarySettings, &tempBuffer, nullptr)))
                {
                    secondaryBuffer = tempBuffer;
                    activeBufferSizeBytes = secondarySettings.dwBufferBytes;

                    // Pre-load the write cursor to give some additional buffering
                    // at the expense of added latency
                    bytesPerSample = waveformSettings.nBlockAlign;
                    writeAheadBytes = (latencyms * samplesPerSecond / 1000) * waveformSettings.nBlockAlign;
                    activeWriteCursor = writeAheadBytes;
                }
            }                    
        }
    }
}
