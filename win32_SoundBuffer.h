#if !defined(WIN32_SOUNDBUFFER_H)
#define WIN32_SOUNDBUFFER_H

class win32SoundBuffer
{
public:

    const i32 samplesPerSecond = 48000;
    const i32 circularBufferSeconds = 3;
    const i32 sampleSizeBits = 16;
    const i32 channelCount = 2;
    void *waveformBuffer = nullptr;

    win32SoundBuffer& operator=(const win32SoundBuffer&) = delete;

    
public:

    void createWaveformBuffer(HWND windowHandle, i32 latencyms);

    void updateDeviceFromWaveform(i32 sampleCount);
    i32  nextCycleSampleCount();
    

private:

    void *secondaryBuffer = nullptr;
    bool playbackActive = false;

    i32 bytesPerSample = 0;
    i32 predictedBytesPerCycle = 0;
    i32 predictedReadCursor = 0;
    i32 writeAheadBytes = 0;
    i32 activeWriteCursor = 0;
    i32 activeBufferSizeBytes = 0;

    
private:

    void initDirectSound(HWND windowHandle, i32 latencyms);
};

#endif
