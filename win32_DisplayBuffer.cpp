
#include "win32_DisplayBuffer.h"

win32DisplayBuffer::Dimensions win32DisplayBuffer::GetDeviceDimensions(HWND windowHandle)
{
    Dimensions result;
    
    RECT clientRect;
    GetClientRect(windowHandle, &clientRect);
    result.width = clientRect.right - clientRect.left;
    result.height = clientRect.bottom - clientRect.top;
    return result;
}

void win32DisplayBuffer::createSimulationBuffer(i32 width, i32 height)
{
    if (simBuffer.pixelData)
    {
        HeapFree(GetProcessHeap(), 0, simBuffer.pixelData);
    }

    simBuffer.width = width;
    simBuffer.height = height;
    simBuffer.bytesPerPixel = 4;
    simBuffer.pitch = width * simBuffer.bytesPerPixel;

    simBuffer.bitmapInfo.bmiHeader.biSize = sizeof(simBuffer.bitmapInfo.bmiHeader);
    simBuffer.bitmapInfo.bmiHeader.biWidth = width;
    simBuffer.bitmapInfo.bmiHeader.biHeight = height;
    simBuffer.bitmapInfo.bmiHeader.biPlanes = 1;
    simBuffer.bitmapInfo.bmiHeader.biBitCount = 32;
    simBuffer.bitmapInfo.bmiHeader.biCompression = BI_RGB;

    i32 bitmapSize = (width * height) * simBuffer.bytesPerPixel;
    simBuffer.pixelData = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, bitmapSize);
}

void win32DisplayBuffer::createDisplayBuffer(i32 width, i32 height)
{
    if (displayBuffer.pixelData)
    {
        HeapFree(GetProcessHeap(), 0, displayBuffer.pixelData);
    }

    displayBuffer.width = width;
    displayBuffer.height = height;
    displayBuffer.bytesPerPixel = 4;
    displayBuffer.pitch = width * displayBuffer.bytesPerPixel;

    displayBuffer.bitmapInfo.bmiHeader.biSize = sizeof(displayBuffer.bitmapInfo.bmiHeader);
    displayBuffer.bitmapInfo.bmiHeader.biWidth = width;
    displayBuffer.bitmapInfo.bmiHeader.biHeight = height;
    displayBuffer.bitmapInfo.bmiHeader.biPlanes = 1;
    displayBuffer.bitmapInfo.bmiHeader.biBitCount = 32;
    displayBuffer.bitmapInfo.bmiHeader.biCompression = BI_RGB;

    i32 bitmapSize = (width * height) * displayBuffer.bytesPerPixel;
    displayBuffer.pixelData = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, bitmapSize);
}

void win32DisplayBuffer::renderSimulationToDisplay(bool halfResolution, i32 width, i32 height)
{
    u8 *srcPixelRef = static_cast<u8 *>(simBuffer.pixelData);
    u8 *dstPixelRef = static_cast<u8 *>(displayBuffer.pixelData);

    i32 modeWidth = simBuffer.width;
    i32 modeHeight = simBuffer.height;
    if (halfResolution)
    {
        modeWidth /= 2;
        modeHeight /= 2;
    }
    
    i32 scaledXRatio = (modeWidth << 16) / width;
    i32 scaledYRatio = (modeHeight << 16) / height;    
    i32 dstYOffset = displayBuffer.height - height;

    for (i32 yLoop = 0; yLoop < height; yLoop++)
    {        
        i32 nearestY = (yLoop * scaledYRatio) >> 16;
        u32 *srcPixels = reinterpret_cast<u32 *>(srcPixelRef + (simBuffer.pitch * nearestY));
        u32 *dstPixels = reinterpret_cast<u32 *>(dstPixelRef + (displayBuffer.pitch * (yLoop + dstYOffset)));

        for (i32 xLoop = 0; xLoop < width; xLoop++)
        {
            i32 nearestX = (xLoop * scaledXRatio) >> 16;
            *dstPixels++ = srcPixels[nearestX];
        }        
    }
}

void win32DisplayBuffer::updateDeviceFromBuffer(HDC deviceContext, i32 width, i32 height)
{
    StretchDIBits(deviceContext,
                  0, 0, width, height,
                  0, 0, displayBuffer.width, displayBuffer.height,
                  displayBuffer.pixelData,
                  &displayBuffer.bitmapInfo,
                  DIB_RGB_COLORS,
                  SRCCOPY);    
}
