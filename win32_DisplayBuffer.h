#if !defined(WIN32_DISPLAYBUFFER_H)
#define WIN32_DISPLAYBUFFER_H

class win32DisplayBuffer
{
public:

    struct Dimensions
    {
        i32 width;
        i32 height;
    };

    struct BufferInfo
    {
        i32 width;
        i32 height;
        i32 bytesPerPixel;
        i32 pitch;

        BITMAPINFO bitmapInfo;
        void *pixelData;
    };

    BufferInfo simBuffer;
    BufferInfo displayBuffer;

    
public:

    Dimensions GetDeviceDimensions(HWND windowHandle);

    void createSimulationBuffer(i32 width, i32 height);
    void createDisplayBuffer(i32 width, i32 height);

    void renderSimulationToDisplay(bool halfResolution, i32 width, i32 height);
    void updateDeviceFromBuffer(HDC deviceContext, i32 width, i32 height);
};

#endif
