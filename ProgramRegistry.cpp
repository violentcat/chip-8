
#include "ProgramRegistry.h"

namespace
{
    const u8 defaultFlags = 
        asType(COMPATIBILITY_FLAGS::LOAD_STORE_INC) |
        asType(COMPATIBILITY_FLAGS::SHIFT_USE_VY) | 
        asType(COMPATIBILITY_FLAGS::WRAP_X_AXIS) | 
        asType(COMPATIBILITY_FLAGS::WRAP_Y_AXIS);
    
    // This table contains descriptor information for default ROM configuration
    // Generally only ROMs that require something different to the default configuration
    // are listed here
    
    const ProgramRegistry::ProgramDescriptor romTable[] =
    {
        {850, {0xe1,0xc8,0x4e,0x11,0x56,0x17,0x40,0x70,0x66,0x1c,0x1f,0x6c,0xa0,0x48,0x1b,0xa5},
         "Blinky", "Hans Christian Egeberg", "1991",
         defaultFlags & ~(asType(COMPATIBILITY_FLAGS::LOAD_STORE_INC) | asType(COMPATIBILITY_FLAGS::SHIFT_USE_VY))},
        
        {850, {0xa6,0x7f,0x58,0x74,0x2c,0xff,0x77,0x70,0x2c,0xc6,0x4c,0x64,0x41,0x3d,0xc3,0x7d},
         "Space Invaders", "David Winter", "",
         defaultFlags & ~asType(COMPATIBILITY_FLAGS::SHIFT_USE_VY)},

        {850, {0x42,0x57,0x6d,0x87,0xb2,0xff,0xb7,0xf4,0xbf,0x4a,0x0f,0x04,0x46,0x04,0x15,0x34},
         "Stars", "Sergey Naydenov", "2010",
         defaultFlags & ~asType(COMPATIBILITY_FLAGS::LOAD_STORE_INC)},

        {850, {0xa7,0xb1,0x71,0xe6,0xf7,0x38,0x91,0x3f,0x89,0x15,0x32,0x62,0xb0,0x15,0x81,0xba},
         "Astro Dodge", "Revival Studios", "2008",
         defaultFlags & ~asType(COMPATIBILITY_FLAGS::LOAD_STORE_INC)},

        {850, {0x81,0x80,0xb8,0x36,0xee,0xb6,0x29,0xba,0x93,0x58,0x35,0x19,0xa5,0xfb,0x7b,0x38},
         "Blitz", "David Winter", "",
         defaultFlags & ~asType(COMPATIBILITY_FLAGS::WRAP_Y_AXIS)},

        {850, {0xb5,0x6e,0x0e,0x6e,0x39,0x30,0x01,0x10,0x49,0xfc,0xf6,0xcf,0x33,0x84,0xe9,0x64},
         "Bowling", "Gooitzen van der Wal", "",
         defaultFlags & ~asType(COMPATIBILITY_FLAGS::WRAP_X_AXIS)},

        {1000, {0xfb,0x32,0x84,0x20,0x5c,0x90,0xd8,0x0c,0x3b,0x17,0xae,0xea,0x2e,0xed,0xf0,0xe4},
         "Blinky SChip", "Hans Christian Egeberg", "1991",
         defaultFlags & ~(asType(COMPATIBILITY_FLAGS::LOAD_STORE_INC) | asType(COMPATIBILITY_FLAGS::SHIFT_USE_VY))},                 
        
        {0, {}, "", "", "", 0}
    };  
    
    const u32 shiftTable[64] = { 7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
                                 5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
                                 4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
                                 6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21 };

    const u32 constantTable[64] = { 0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
                                    0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
                                    0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
                                    0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
                                    0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
                                    0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
                                    0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
                                    0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
                                    0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
                                    0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
                                    0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
                                    0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
                                    0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
                                    0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
                                    0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
                                    0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 };

    u32 rotShiftLeft(u32 function, u32 shift)
    {
        return ((function << shift) | (function >> (32 - shift)));
    }    
};

void ProgramRegistry::generateMD5(void *programData, u32 programSize, u32 alignedSize, MD5Digest *md5Hash)
{
    u32 streamIndex = programSize;

    // Although the MD5 algorithm considers the input as bit strings, we know
    // the input is actually provided in entire 8bit bytes so at least in the case
    // of pre-processing we can work in bytes (although the original program size
    // appended to the stream is number of bits)

    // Pre-processing stage (padding)
    
    u8 *inputStream = reinterpret_cast<u8 *>(programData);
    inputStream[streamIndex++] = 0x80;
    
    while (streamIndex < (alignedSize - 8))
    {
        inputStream[streamIndex++] = 0x00;
    }

    *(reinterpret_cast<u64 *>(&inputStream[streamIndex])) = static_cast<u64>(programSize) * 8;

    // Main hash algorithm

    u32 initA = 0x67452301;
    u32 initB = 0xefcdab89;
    u32 initC = 0x98badcfe;
    u32 initD = 0x10325476;    

    u32 *chunkStream = reinterpret_cast<u32 *>(programData);

    u32 currentChunk = 0;
    while (currentChunk < alignedSize)
    {
        u32 chunkElement[16];
        for (i32 loop = 0; loop < 16; loop++)
        {
            chunkElement[loop] = chunkStream[loop];
        }

        u32 stateA = initA;
        u32 stateB = initB;
        u32 stateC = initC;
        u32 stateD = initD;

        for (i32 loop = 0; loop < 64; loop++)
        {
            u32 funcResult;
            u32 elementSelect;
            
            if (loop < 16)
            {
                funcResult = (stateB & stateC) | (~stateB & stateD);
                elementSelect = loop;
            }
            else if (loop < 32)
            {
                funcResult = (stateD & stateB) | (~stateD & stateC);
                elementSelect = (loop * 5 + 1) % 16;
            }
            else if (loop < 48)
            {
                funcResult = stateB ^ stateC ^ stateD;
                elementSelect = (loop * 3 + 5) % 16;
            }
            else
            {
                funcResult = stateC ^ (stateB | ~stateD);
                elementSelect = (loop * 7) % 16;
            }

            funcResult = funcResult + stateA + constantTable[loop] + chunkElement[elementSelect];
            stateA = stateD;
            stateD = stateC;
            stateC = stateB;
            stateB = stateB + rotShiftLeft(funcResult, shiftTable[loop]);
        }

        initA = initA + stateA;
        initB = initB + stateB;
        initC = initC + stateC;
        initD = initD + stateD;
        
        chunkStream += 16;
        currentChunk += 64;
    }

    u32 *digestStore = reinterpret_cast<u32 *>(md5Hash->block);
    *digestStore++ = initA;
    *digestStore++ = initB;
    *digestStore++ = initC;
    *digestStore++ = initD;    
}

void ProgramRegistry::configureSimulation(MD5Digest *md5Hash, u8 *flags, i32 *cyclesPerSecond)
{
    i32 registryCycles = 850;
    u8 registryFlags = defaultFlags;
    
    i32 romIndex = 0;
    for(;; romIndex++)
    {
        if (romTable[romIndex].cyclesPerSecond == 0)
            break;

        bool matchMD5 = true;
        for (i32 loop = 0; loop < 16; loop++)
        {
            if (md5Hash->block[loop] != romTable[romIndex].hash.block[loop])
            {
                matchMD5 = false;
                break;
            }
        }

        if (matchMD5 == true)
        {
            registryCycles = romTable[romIndex].cyclesPerSecond;
            registryFlags = romTable[romIndex].flags;
            break;
        }
    }

    *flags = registryFlags;
    *cyclesPerSecond = registryCycles;
}

